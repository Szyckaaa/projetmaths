package modele.fraction.exception;

public class InversionNulException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = -3597643935039105730L;

    public InversionNulException( String message ) {
        super( message );
    }
}
