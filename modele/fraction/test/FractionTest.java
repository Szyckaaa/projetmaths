package modele.fraction.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modele.fraction.Fraction;
import modele.fraction.exception.InversionNulException;

public class FractionTest {

    Fraction f;

    @Before
    public void setUp() throws Exception {
        f = new Fraction( 1, 4 );
    }

    @After
    public void tearDown() throws Exception {
        f = null;
    }

    @Test
    public void testConstructUnArgument() {
        f = new Fraction( 4, 1 );
        Fraction a = new Fraction( 4 );
        assertEquals( f, a );
    }

    @Test
    public void testGetNumerator() {
        assertEquals( 1, f.getNumerator() );
    }

    @Test
    public void testGetDenominator() {
        assertEquals( 4, f.getDenominator() );
    }

    @Test
    public void testSetNumerator() {
        f.setNumerator( 3 );
        assertEquals( 3, f.getNumerator() );
    }

    @Test
    public void testSetDenominator() {
        f.setDenominator( 8 );
        assertEquals( 8, f.getDenominator() );
    }

    @Test
    public void testGetSigne() {
        assertEquals( 1, f.getSigne() );
        f = new Fraction( -1, 4 );
        assertEquals( -1, f.getSigne() );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testSetDenominatorException() {
        f.setDenominator( 0 );
    }

    @Test
    public void testCalculateGCD() {
        assertEquals( 15, Fraction.calculateGCD( 15, 45 ) );
        assertEquals( 1, Fraction.calculateGCD( 16, 45 ) );
    }

    @Test
    public void testReduce() {
        f.setNumerator( 15 );
        f.setDenominator( 45 );
        f.reduce();
        assertEquals( 1, f.getNumerator() );
        assertEquals( 3, f.getDenominator() );
    }

    @Test
    public void testAddWithoutReduce() {
        f = new Fraction( 1, 3 );
        Fraction a = new Fraction( 1, 3 );
        f = f.add( a );
        assertEquals( 1, f.getSigne() );
        assertEquals( 2, f.getNumerator() );
        assertEquals( 3, f.getDenominator() );
    }

    @Test
    public void testAddWithReduce() {
        f = new Fraction( 1, 4 );
        Fraction a = new Fraction( 1, 4 );
        f = f.add( a );
        assertEquals( 1, f.getSigne() );
        assertEquals( 1, f.getNumerator() );
        assertEquals( 2, f.getDenominator() );
    }

    @Test
    public void testOneNegativeAdd() {
        f = new Fraction( 3, 4 );
        Fraction a = new Fraction( -1, 4 );
        f = f.add( a );
        assertEquals( 1, f.getSigne() );
        assertEquals( 1, f.getNumerator() );
        assertEquals( 2, f.getDenominator() );
    }

    @Test
    public void testBothNegativeAdd() {
        f = new Fraction( 3, -4 );
        Fraction a = new Fraction( -1, 4 );
        f = f.add( a );
        assertEquals( -1, f.getSigne() );
        assertEquals( 1, f.getNumerator() );
        assertEquals( 1, f.getDenominator() );
    }

    @Test
    public void testSubstractWithoutReduce() {
        f = new Fraction( 1, 3 );
        Fraction a = new Fraction( 2, 3 );
        f = f.subtract( a );
        assertEquals( -1, f.getSigne() );
        assertEquals( 1, f.getNumerator() );
        assertEquals( 3, f.getDenominator() );
    }

    @Test
    public void testSubstractWithReduce() {
        f = new Fraction( 1, 4 );
        Fraction a = new Fraction( 1, 4 );
        f = f.subtract( a );
        assertEquals( 1, f.getSigne() );
        assertEquals( 0, f.getNumerator() );
        assertEquals( 1, f.getDenominator() );
    }

    @Test
    public void testOneNegativeSubstract() {
        f = new Fraction( 3, 4 );
        Fraction a = new Fraction( -1, 4 );
        f = f.subtract( a );
        assertEquals( 1, f.getSigne() );
        assertEquals( 1, f.getNumerator() );
        assertEquals( 1, f.getDenominator() );
    }

    @Test
    public void testBothNegativeSubstract() {
        f = new Fraction( 3, -4 );
        Fraction a = new Fraction( -1, 4 );
        f = f.subtract( a );
        assertEquals( -1, f.getSigne() );
        assertEquals( 1, f.getNumerator() );
        assertEquals( 2, f.getDenominator() );
    }

    @Test
    public void testMultiply() {
        Fraction a = new Fraction( 3, 2 );
        f = f.multiply( a );
        assertEquals( 1, f.getSigne() );
        assertEquals( 3, f.getNumerator() );
        assertEquals( 8, f.getDenominator() );
    }

    @Test
    public void testOneNegativeMultiply() {
        Fraction a = new Fraction( -3, 2 );
        f = f.multiply( a );
        assertEquals( -1, f.getSigne() );
        assertEquals( 3, f.getNumerator() );
        assertEquals( 8, f.getDenominator() );
    }

    @Test
    public void testBothNegativeMultiply() {
        f = new Fraction( 1, -4 );
        Fraction a = new Fraction( -3, 2 );
        f = f.multiply( a );
        assertEquals( 1, f.getSigne() );
        assertEquals( 3, f.getNumerator() );
        assertEquals( 8, f.getDenominator() );
    }

    @Test
    public void testDivide() {
        Fraction a = new Fraction( 3, 2 );
        f = f.divide( a );
        assertEquals( 1, f.getSigne() );
        assertEquals( 1, f.getNumerator() );
        assertEquals( 6, f.getDenominator() );
    }

    @Test
    public void testOneNegativeDivide() {
        Fraction a = new Fraction( -3, 2 );
        f = f.divide( a );
        assertEquals( -1, f.getSigne() );
        assertEquals( 1, f.getNumerator() );
        assertEquals( 6, f.getDenominator() );
    }

    @Test
    public void testBothNegativeDivide() {
        f = new Fraction( 1, -4 );
        Fraction a = new Fraction( -3, 2 );
        f = f.divide( a );
        assertEquals( 1, f.getSigne() );
        assertEquals( 1, f.getNumerator() );
        assertEquals( 6, f.getDenominator() );
    }

    @Test
    public void testInverse() throws InversionNulException {
        Fraction f = new Fraction( 3, 2 );
        f = f.inverse();
        assertEquals( 1, f.getSigne() );
        assertEquals( 2, f.getNumerator() );
        assertEquals( 3, f.getDenominator() );
    }

    @Test
    public void testInverseNegatif() throws InversionNulException {
        Fraction f = new Fraction( 3, -2 );
        f = f.inverse();
        assertEquals( -1, f.getSigne() );
        assertEquals( 2, f.getNumerator() );
        assertEquals( 3, f.getDenominator() );
    }

    @Test( expected = InversionNulException.class )
    public void testInverseException() throws InversionNulException {
        Fraction f = new Fraction( 0, 2 );
        f = f.inverse();
    }

    @Test
    public void testToStringPositif() {
        Fraction f = new Fraction( 3, 2 );
        assertEquals( "[3/2]", f.toString() );
    }

    @Test
    public void testToStringNegatif() {
        Fraction f = new Fraction( 3, -2 );
        assertEquals( "[-3/2]", f.toString() );
    }

    @Test
    public void testToStringEntier() {
        Fraction f = new Fraction( 3 );
        assertEquals( "[3]", f.toString() );
    }

    @Test
    public void testToStringEntierNegatif() {
        Fraction f = new Fraction( -3 );
        assertEquals( "[-3]", f.toString() );
    }
}
