package modele.fraction;

import modele.fraction.exception.InversionNulException;

public class Fraction {

    long numerator;
    long denominator;
    /**
     * Signe, 1 = +, -1 = -
     */
    int  signe;

    /**
     * 
     * @param numr
     * @param denr
     * @throws IllegalArgumentException
     *             si denominator vaut 0
     * @see setDenominator
     */
    public Fraction( long numr, long denr ) throws IllegalArgumentException {
        this.setNumerator( Math.abs( numr ) );
        this.setDenominator( Math.abs( denr ) );
        if ( numr * denr >= 0 )
            this.signe = 1;
        else
            this.signe = -1;
        reduce();
    }

    public Fraction( long numr ) {
        this.setNumerator( Math.abs( numr ) );
        this.setDenominator( 1 );
        if ( numr >= 0 )
            this.signe = 1;
        else
            this.signe = -1;
    }

    public long getNumerator() {
        return numerator;
    }

    public void setNumerator( long numerator ) {
        this.numerator = numerator;
    }

    public long getDenominator() {
        return denominator;
    }

    /**
     * 
     * @param denominator
     * @throws IllegalArgumentException
     *             si denominator vaut 0
     */
    public void setDenominator( long denominator ) throws IllegalArgumentException {
        if ( denominator == 0 )
            throw new IllegalArgumentException( "Impossible d'avoir une fraction avec 0 comme denominateur" );

        this.denominator = denominator;
    }

    public int getSigne() {
        return this.signe;
    }

    /**
     * Calculates gcd of two numbers
     * 
     * @param numerator
     * @param denominator
     * @return le plus grand diviseur commun des deux termes
     */
    public static long calculateGCD( long numerator, long denominator ) {
        if ( numerator % denominator == 0 ) {
            return denominator;
        }
        return Fraction.calculateGCD( denominator, numerator % denominator );
    }

    /**
     * Reduce the fraction to lowest form
     */
    public void reduce() {
        long gcd = Fraction.calculateGCD( numerator, denominator );
        numerator /= gcd;
        denominator /= gcd;
    }

    /**
     * Adds two fractions
     * 
     * @param fractionTwo
     * @return fraction courante plus fractionTwo
     */
    public Fraction add( Fraction fractionTwo ) {
        long numer = ( this.getSigne() * numerator * fractionTwo.getDenominator() )
                + ( fractionTwo.getSigne() * fractionTwo.getNumerator() * denominator );
        long denr = denominator * fractionTwo.getDenominator();
        return new Fraction( numer, denr );
    }

    /**
     * Subtracts two fractions
     * 
     * @param fractionTwo
     * @return fraction courante moins fractionTwo
     */
    public Fraction subtract( Fraction fractionTwo ) {
        long newNumerator = ( this.getSigne() * numerator * fractionTwo.denominator )
                - ( fractionTwo.getSigne() * fractionTwo.numerator * denominator );
        long newDenominator = denominator * fractionTwo.denominator;
        Fraction result = new Fraction( newNumerator, newDenominator );
        return result;
    }

    /**
     * Multiples two functions
     * 
     * @param fractionTwo
     * @returnla fraction courante multipli�e par fractionTwo
     */
    public Fraction multiply( Fraction fractionTwo ) {
        long newNumerator = this.signe * numerator * fractionTwo.numerator * fractionTwo.getSigne();
        long newDenominator = denominator * fractionTwo.denominator;
        Fraction result = new Fraction( newNumerator, newDenominator );
        return result;
    }

    /**
     * Divides two fractions
     * 
     * @param fractionTwo
     * @return la fraction courante divis�e par fractionTwo
     */
    public Fraction divide( Fraction fractionTwo ) {
        long newNumerator = this.signe * numerator * fractionTwo.getSigne() * fractionTwo.getDenominator();
        long newDenominator = denominator * fractionTwo.numerator;
        Fraction result = new Fraction( newNumerator, newDenominator );
        return result;
    }

    /**
     * Inverse la fraction
     * 
     * @return l'inverse de la fraction courante
     * @throws IllegalArgumentException
     *             si le num�rateur vaut 0
     */
    public Fraction inverse() throws InversionNulException {
        try {
            return new Fraction( this.denominator, this.signe * this.numerator );
        } catch ( IllegalArgumentException e ) {
            throw new InversionNulException(
                    "L'inversion de la fraction est impossible car la fraction r�sultante a une d�nominateur � 0" );
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) ( denominator ^ ( denominator >>> 32 ) );
        result = prime * result + (int) ( numerator ^ ( numerator >>> 32 ) );
        result = prime * result + signe;
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        Fraction other = (Fraction) obj;
        if ( denominator != other.denominator )
            return false;
        if ( numerator != other.numerator )
            return false;
        if ( signe != other.signe )
            return false;
        return true;
    }

    @Override
    public String toString() {
        String signe = "";
        if ( this.signe == -1 )
            signe = "-";

        String denom = "";
        if ( this.denominator != 1 )
            denom = "/" + this.denominator;

        return "[" + signe + this.numerator + denom + "]";
    }

    public String toString( boolean withBracket ) {
        String result = "";
        String signe = "";
        if ( this.signe == -1 )
            signe = "-";

        String denom = "";
        if ( this.denominator != 1 )
            denom = "/" + this.denominator;

        result = "[" + signe + this.numerator + denom + "]";
        if ( !withBracket )
            result = result.substring( 1, result.length() - 1 );

        return result;
    }

}
