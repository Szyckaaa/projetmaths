package modele;
import modele.fraction.Fraction;
import modele.fraction.exception.InversionNulException;
import modele.matrice.MatriceCarre;
import modele.matrice.exception.HorsDePorteeException;
import modele.matrice.exception.OperationInvalideException;

public class khaletski {

    public static void main( String[] args )
            throws OperationInvalideException, HorsDePorteeException, InversionNulException {
        exampleForApp();
    }

    public static void exampleForApp() throws OperationInvalideException, HorsDePorteeException {
        /*
         * L'objectif est de r�soudre une �quation de la forme A . X = B o� A
         * est une matrice de taille n, X un vecteur inconnue de n ligne et B un
         * vecteur connue de taille n
         * 
         * On peut manipuler l'�quation pour obtenir l'expression de X : X =
         * A^-1 . B
         */

        // La premi�re �tape consiste � d�clarer A et l'initialiser
        MatriceCarre A = new MatriceCarre( 4 );
        Fraction[] coefs = { new Fraction( 3 ), new Fraction( 1 ), new Fraction( -1 ),
                new Fraction( 2 ), new Fraction( -5 ), new Fraction( 1 ), new Fraction( 3 ), new Fraction( -4 ),
                new Fraction( 2 ), new Fraction( 0 ), new Fraction( 1 ), new Fraction( -1 ), new Fraction( 1 ),
                new Fraction( -5 ), new Fraction( 3 ), new Fraction( -3 ) };
        A.setCoefficients( coefs );
        System.out.println( "Matrice A :\n" + A );

        // De A on d�duit la taille de B
        Fraction[] B = new Fraction[A.taille()];
        // on rempli B
        B[0] = new Fraction( 1 );
        B[1] = new Fraction( 2 );
        B[2] = new Fraction( 3 );
        B[3] = new Fraction( 4 );

        int i = 1;
        for ( Fraction f : B ) {
            System.out.println( "B" + i + ":" + f );
            i++;
        }

        // On applique Khaletski sur la matrice A pour r�cup�rer les matrices
        // triangulaire B et C
        MatriceCarre[] resultKhal = new MatriceCarre[2];
        try {
            resultKhal = A.khaletski( B );
        } catch ( InversionNulException e ) {
            e.printStackTrace();
        }
        MatriceCarre b = resultKhal[0];
        MatriceCarre c = resultKhal[1];

        System.out.println( "\nB :\n" + b );
        System.out.println( "C :\n" + c );
        /*
         * khaletski initialise alors le d�terminant de A qui est le determinant
         * de B multiplier par celui de C Or les matrices sont triangulaires. On
         * peut montrer que le det() d'une matrice triangulaire et le produit de
         * ses coefficients diagonaux C ayany une diagonale � 1 son d�terminant
         * et 1. Au final le d�terminant de A est le d�terminant de B
         */
        System.out.println( "\nD�terminant de A : " + A.getDeterminant() );

        System.out.println();
        // D�s lors on r�sout l'�quation B*y=b
        Fraction[] y = MatriceCarre.solveTriangInf( b, B );
        i = 1;
        for ( Fraction f : y ) {
            System.out.println( "Y" + i + ":" + f );
            i++;
        }
        System.out.println();
        // Puis on r�sout C*X=y
        Fraction[] X = MatriceCarre.solveTriangSup( c, y );
        i = 1;
        for ( Fraction f : X ) {
            System.out.println( "X" + i + ":" + f );
            i++;
        }

        // Une v�rification est alors possible :
        // Et v�rifier qu'elle est correct
        Fraction[] check = MatriceCarre.multiplier( A, X );

        System.out.println( "\nV�rification, r�sultat de A.X :" );
        i = 1;
        for ( Fraction f : check ) {
            System.out.println( "B" + i + ":" + f );
            i++;
        }
    }

}
