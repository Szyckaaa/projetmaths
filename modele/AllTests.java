package modele;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith( Suite.class )
@SuiteClasses( { modele.matrice.test.MatriceCarreTest.class, modele.fraction.test.FractionTest.class } )
public class AllTests {

}
