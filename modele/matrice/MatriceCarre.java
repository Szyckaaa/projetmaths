package modele.matrice;

import java.util.Arrays;

import modele.fraction.Fraction;
import modele.fraction.exception.InversionNulException;
import modele.matrice.exception.HorsDePorteeException;
import modele.matrice.exception.IninversibleException;
import modele.matrice.exception.OperationInvalideException;

public class MatriceCarre {

    /**
     * Liste des coefficients de la matrice
     */
    private Fraction     coefficients[][];

    /**
     * D�terminant de la matrice calcul� � l'issue de khaletski ou fadeev
     * R�initialis� � l'appel de setCoefficient
     */
    private Fraction     determinant;

    private Fraction[]   polCaracteristique;

    private MatriceCarre inverse;

    /**
     * Constructeur de la classe
     * 
     * @param taille
     *            Taille de la matrice
     * @throws IllegalArgumentException
     *             Si la taille est n�ative ou nulle
     */
    public MatriceCarre( int taille ) throws IllegalArgumentException {
        if ( taille <= 0 )
            throw new IllegalArgumentException( "La taille de la matrice doit �tre strictement positive" );

        this.determinant = null;

        this.coefficients = new Fraction[taille][taille];
        for ( int i = 1; i <= this.taille(); i++ )
            for ( int j = 1; j <= this.taille(); j++ ) {
                try {
                    this.setCoefficient( i, j, new Fraction( 0, 1 ) );
                } catch ( HorsDePorteeException e ) {
                    e.printStackTrace();
                }
            }
    }

    /**
     * @return taille de la matrice
     */
    public int taille() {
        return this.coefficients.length;
    }

    /**
     * 
     * @param ligne
     * @param colonne
     * @return coefficent de la matrice a(ligne,colonne)
     * @throws HorsDePorteeException
     *             si ligne ou colonne ne correspondent pas � la taille de la
     *             matrice
     */
    public Fraction getCoefficient( int ligne, int colonne ) throws HorsDePorteeException {
        if ( ligne > this.taille() || ligne <= 0 || colonne > this.taille() || colonne <= 0 )
            throw new HorsDePorteeException( ligne, colonne, this.taille() );

        return this.coefficients[ligne - 1][colonne - 1];
    }

    /**
     * Ins�re un coefficient dans la matrice
     * 
     * @param ligne
     * @param colonne
     * @param valeur
     * @throws HorsDePorteeException
     *             si ligne ou colonne ne correspondent pas � la taille de la
     *             matrice
     */
    public void setCoefficient( int ligne, int colonne, Fraction valeur ) throws HorsDePorteeException {
        if ( ligne > this.taille() || ligne <= 0 || colonne > this.taille() || colonne <= 0 )
            throw new HorsDePorteeException( ligne, colonne, this.taille() );

        this.coefficients[ligne - 1][colonne - 1] = valeur;
        this.determinant = null;
        this.inverse = null;
        this.polCaracteristique = null;
    }

    /**
     * Ins�re un ensemble de coeffcient dans la matrice
     * 
     * @param valeurs
     * @throws OperationInvalideException
     *             si le nombre de coefficients en param�tres ne corresponds pas
     *             au nombre de coefficients de la matrice
     * @throws HorsDePorteeException
     * @see {@link #setCoefficient(int, int, Fraction)}
     */
    public void setCoefficients( Fraction[] valeurs ) throws OperationInvalideException, HorsDePorteeException {
        if ( valeurs.length != this.taille() * this.taille() )
            throw new OperationInvalideException( "Impossible d'ins�rer les " + valeurs.length
                    + " coefficients dans une matrice de " + this.taille() * this.taille() + " coefficients" );
        for ( int i = 0; i < valeurs.length; i++ )
            this.setCoefficient( ( i / this.taille() ) + 1, ( i % this.taille() ) + 1, valeurs[i] );
    }

    /**
     * 
     * @return le d�terminant de la matrice
     * @throws NullPointerException
     *             si le determinant n'a pas �t� calcul�
     */
    public Fraction getDeterminant() throws NullPointerException {
        if ( this.determinant == null )
            throw new NullPointerException( "Le d�terminant n'a pas �t� calcul�" );

        return this.determinant;
    }

    /**
     * 
     * @return la matrice inverse de la matrice courante
     * @throws NullPointerException
     *             si l'inverse n'a pas �t� calcul�
     */
    public MatriceCarre getInverse() throws NullPointerException {
        if ( this.inverse == null )
            throw new NullPointerException( "La matrice inverse n'a pas �t� calcul�" );

        return this.inverse;
    }

    /**
     * 
     * @return le polyn�me caract�ristique de la matrice
     * @throws NullPointerException
     *             si le polyn�me caract�ristique n'a pas �t� calcul�
     */
    public Fraction[] getPolCaracteristique() throws NullPointerException {
        if ( this.polCaracteristique == null )
            throw new NullPointerException( "Le polynome caract�ristique n'a pas �t� calcul�" );

        return this.polCaracteristique;
    }

    /**
     * Additionne deux matrices
     * 
     * @param a
     * @param b
     * @return Somme de a et b
     * @throws OperationInvalideException
     *             si les tailles de a et b ne sont pas identiques
     * @throws HorsDePorteeException
     * @see {@link #setCoefficient(int, int, Fraction)}
     * @see {@link #getCoefficient(int, int)}
     */
    public static MatriceCarre additionner( MatriceCarre a, MatriceCarre b )
            throws OperationInvalideException, HorsDePorteeException {
        if ( a.taille() != b.taille() )
            throw new OperationInvalideException( "Tailles des matrices diff�rentes" );

        MatriceCarre result = new MatriceCarre( a.taille() );
        for ( int i = 1; i <= a.taille(); i++ )
            for ( int j = 1; j <= a.taille(); j++ ) {
                result.setCoefficient( i, j, a.getCoefficient( i, j ).add( b.getCoefficient( i, j ) ) );
            }

        return result;
    }

    /**
     * Multiplie deux matrices
     * 
     * @param a
     * @param b
     * @return la mutiplication de a par b
     * @throws OperationInvalideException
     *             si les tailles de a et b ne sont pas identiques
     * @throws HorsDePorteeException
     * @see {@link #setCoefficient(int, int, Fraction)}
     * @see {@link #getCoefficient(int, int)}
     */
    public static MatriceCarre multiplier( MatriceCarre a, MatriceCarre b )
            throws OperationInvalideException, HorsDePorteeException {
        if ( a.taille() != b.taille() )
            throw new OperationInvalideException( "Tailles des matrices diff�rentes" );

        MatriceCarre result = new MatriceCarre( a.taille() );

        // Parcours des coefficients du r�sultat
        for ( int i = 1; i <= a.taille(); i++ )
            for ( int j = 1; j <= a.taille(); j++ ) {
                Fraction tamp = new Fraction( 0, 1 );
                for ( int k = 1; k <= a.taille(); k++ ) {
                    tamp = tamp.add( a.getCoefficient( i, k ).multiply( b.getCoefficient( k, j ) ) );
                }
                result.setCoefficient( i, j, tamp );
            }

        return result;
    }

    /**
     * Effectue le produit matriciel a.m
     * 
     * @param a
     * @param x
     * @return le r�sultat du produit matriciel a.m
     * @throws OperationInvalideException
     * @throws HorsDePorteeException
     */
    public static Fraction[] multiplier( MatriceCarre a, Fraction[] x )
            throws OperationInvalideException, HorsDePorteeException {
        if ( a.taille() != x.length )
            throw new OperationInvalideException(
                    "La taille de matrice est de " + a.taille() + " et celle du vecteur est de " + x.length );

        Fraction[] result = new Fraction[a.taille()];

        // Parcours des coefficients du r�sultat
        for ( int i = 1; i <= a.taille(); i++ ) {
            Fraction tamp = new Fraction( 0, 1 );
            for ( int j = 1; j <= a.taille(); j++ ) {

                tamp = tamp.add( a.getCoefficient( i, j ).multiply( x[j - 1] ) );

            }
            result[i - 1] = tamp;
        }
        return result;
    }

    /**
     * Multiplie une matrice par un r�el
     * 
     * @param a
     * @param valeur
     * @return la multiplication de a par valeur
     * @throws HorsDePorteeException
     * @see {@link #setCoefficient(int, int, Fraction)}
     * @see {@link #getCoefficient(int, int)}
     */
    public static MatriceCarre multiplier( MatriceCarre a, Fraction valeur )
            throws HorsDePorteeException {

        MatriceCarre result = new MatriceCarre( a.taille() );

        // Parcours des coefficients du r�sultat
        for ( int i = 1; i <= a.taille(); i++ )
            for ( int j = 1; j <= a.taille(); j++ ) {
                result.setCoefficient( i, j, a.getCoefficient( i, j ).multiply( valeur ) );
            }

        return result;
    }

    /**
     * @return la trace de la matrice
     * @throws HorsDePorteeException
     * @see {@link #getCoefficient(int, int)}
     */
    public Fraction trace() throws HorsDePorteeException {
        Fraction result = new Fraction( 0, 1 );
        for ( int i = 0; i < this.taille(); i++ )
            result = result.add( this.getCoefficient( i + 1, i + 1 ) );

        return result;
    }

    /**
     * 
     * @param taille
     * @return la matrice identit� de la taille pass�e en param�tre
     * @throws HorsDePorteeException
     * @see {@link #setCoefficient(int, int, Fraction)}
     */
    public static MatriceCarre genererMatriceIdentite( int taille ) throws HorsDePorteeException {
        MatriceCarre m = new MatriceCarre( taille );
        for ( int i = 0; i < m.taille(); i++ )
            m.setCoefficient( i + 1, i + 1, new Fraction( 1, 1 ) );

        return m;
    }

    /**
     * Applique l'algorithme de Fadeev pour instancier les attribut inverse,
     * determinant et polynome caract�ristique
     * 
     * 
     * @throws IninversibleException
     */
    public void fadeev() throws IninversibleException {
        MatriceCarre zero = new MatriceCarre( this.taille() );

        MatriceCarre a = new MatriceCarre( this.taille() );
        a.coefficients = this.coefficients;

        Fraction[] polCar = new Fraction[this.taille() + 1];
        polCar[0] = new Fraction( 1 );

        MatriceCarre b = null;
        try {
            b = MatriceCarre.genererMatriceIdentite( this.taille() );
        } catch ( HorsDePorteeException e ) {
            e.printStackTrace();
        }

        MatriceCarre l = null;

        Fraction q = new Fraction( 0 );

        int n = 1;
        do {
            try {
                a = MatriceCarre.multiplier( this, b );
                q = a.trace().divide( new Fraction( n ) );
                l = b;
                polCar[n] = q.multiply( new Fraction( -1 ) );
                b = MatriceCarre.additionner( a,
                        MatriceCarre.multiplier( MatriceCarre.genererMatriceIdentite( this.taille() ),
                                q.multiply( new Fraction( -1 ) ) ) );
                n++;
            } catch ( OperationInvalideException | HorsDePorteeException e ) {
                e.printStackTrace();
            }
        } while ( !b.equals( zero ) );

        try {
            if ( n % 2 == 1 )
                this.determinant = q.multiply( new Fraction( -1 ) );
            else
                this.determinant = q;
            this.polCaracteristique = polCar;
            this.inverse = MatriceCarre.multiplier( l, q.inverse() );
        } catch ( HorsDePorteeException e ) {
            e.printStackTrace();
        } catch ( InversionNulException e ) {
            throw new IninversibleException();
        }
    }

    /**
     * Surchage de {@link #fadeev()} o� les param�tres r�cup�rent les calculs
     * interm�diaires
     * 
     * @param lesA
     *            tableau r�cup�rant les matrices A
     * @param lesQ
     *            tableau r�cup�rant les r�els q
     * @param lesB
     *            tableau r�cup�rant les matrices B
     *
     * @throws OperationInvalideException
     * @throws IninversibleException
     * @see {@link #fadeev()}
     */
    public void fadeev( MatriceCarre[] lesA, Fraction[] lesQ, MatriceCarre[] lesB )
            throws OperationInvalideException, IninversibleException {
        if ( lesA.length != this.taille() || lesB.length != this.taille() || lesQ.length != this.taille() )
            throw new OperationInvalideException(
                    "Les variables de sauvegarde des calculs interm�diaire ne sont pas de dimension " + this.taille() );

        MatriceCarre zero = new MatriceCarre( this.taille() );

        MatriceCarre a = new MatriceCarre( this.taille() );
        a.coefficients = this.coefficients;

        MatriceCarre b = null;
        try {
            b = MatriceCarre.genererMatriceIdentite( this.taille() );
        } catch ( HorsDePorteeException e ) {
            e.printStackTrace();
        }

        MatriceCarre l = null;

        Fraction q = new Fraction( 0 );

        Fraction[] polCar = new Fraction[this.taille() + 1];
        polCar[0] = new Fraction( 1 );

        int n = 1;
        do {
            try {
                a = MatriceCarre.multiplier( this, b );
                q = a.trace().divide( new Fraction( n ) );
                l = b;
                b = MatriceCarre.additionner( a,
                        MatriceCarre.multiplier( MatriceCarre.genererMatriceIdentite( this.taille() ),
                                q.multiply( new Fraction( -1 ) ) ) );
                polCar[n] = q.multiply( new Fraction( -1 ) );
                lesA[n - 1] = a.clone();
                lesB[n - 1] = b.clone();
                lesQ[n - 1] = new Fraction( q.getSigne() * q.getNumerator(), q.getDenominator() );
                n++;
            } catch ( OperationInvalideException | HorsDePorteeException e ) {
                e.printStackTrace();
            }
        } while ( !b.equals( zero ) );

        try {
            this.determinant = q;
            this.inverse = MatriceCarre.multiplier( l, q.inverse() );
            this.polCaracteristique = polCar;
        } catch ( HorsDePorteeException e ) {
            e.printStackTrace();
        } catch ( InversionNulException e ) {
            throw new IninversibleException();
        }
    }

    /**
     * Applique l'algorithme de khaletski pour scinder la matrice courante en
     * deux matrices triangulaires solution de l'�quation A.X=B
     * 
     * @param secTerme
     * @return Un tableau de MatriceCarre o� [0] est B et [1] est C
     * @throws HorsDePorteeException
     *             voir {@link #getCoefficient(int, int)}
     * @throws InversionNulException
     *             voir {@link Fraction.inverse()}
     * @throws OperationInvalideException*
     *             si secTerme n'est pas un vecteur de this.taille() colonnes
     */
    public MatriceCarre[] khaletski( Fraction[] secTerme )
            throws HorsDePorteeException, InversionNulException, OperationInvalideException {
        if ( secTerme.length != this.taille() )
            throw new OperationInvalideException( "La dimension du second terme est de " + secTerme.length
                    + " alors qu'elle doit �tre �gale � " + this.taille() + " pour appliquer la m�thode de khaletski" );

        MatriceCarre b = new MatriceCarre( this.taille() );
        MatriceCarre c = new MatriceCarre( this.taille() );
        for ( int i = 1; i <= this.taille(); i++ )
            c.setCoefficient( i, i, new Fraction( 1 ) );

        // Boucle de remplissage
        for ( int iteration = 1; iteration <= this.taille(); iteration++ ) {
            // Initialisation
            if ( iteration == 1 ) {
                for ( int j = 1; j <= this.taille(); j++ ) {
                    b.setCoefficient( j, 1, this.getCoefficient( j, 1 ) );
                    c.setCoefficient( 1, j, this.getCoefficient( 1, j ).divide(
                            b.getCoefficient( 1, 1 ) ) );
                }
            } else {
                // remplir la ligne de B
                for ( int j = 2; j <= iteration; j++ ) {
                    Fraction sumB = new Fraction( 0 );
                    for ( int k = 1; k < j; k++ )
                        sumB = sumB.add( b.getCoefficient( iteration, k ).multiply(
                                c.getCoefficient( k, j ) ) );

                    b.setCoefficient( iteration, j, this.getCoefficient( iteration, j ).subtract( sumB ) );
                }
                // remplir la colonne de C
                if ( iteration != this.taille() ) {
                    for ( int j = iteration + 1; j <= this.taille(); j++ ) {
                        Fraction sumC = new Fraction( 0 );
                        for ( int k = 1; k < iteration; k++ )
                            sumC = sumC.add( b.getCoefficient( iteration, k ).multiply(
                                    c.getCoefficient( k, j ) ) );
                        c.setCoefficient( iteration, j,
                                b.getCoefficient( iteration, iteration ).inverse().multiply(
                                        this.getCoefficient( iteration, j ).subtract( sumC ) ) );
                    }
                }
            }

        }
        // Calcul du d�terminant
        Fraction determinant = new Fraction( 1 );
        for ( int i = 1; i <= b.taille(); i++ )
            determinant = determinant.multiply( b.getCoefficient( i, i ) );

        this.determinant = determinant;

        MatriceCarre[] result = { b, c };
        return result;
    }

    /**
     * Surcharge de {@link #khaletski(Fraction[])} o� les matrices
     * interm�diaires sont stock�s dans des tableaux afin d'�tre r�cup�r�s
     * 
     * @param secTerme
     * @return Un tableau de MatriceCarre o� [0] est B et [1] est C
     * @throws HorsDePorteeException
     *             voir {@link #getCoefficient(int, int)}
     * @throws InversionNulException
     *             voir {@link Fraction.inverse()}
     * @throws OperationInvalideException*
     *             si secTerme n'est pas un vecteur de this.taille() colonnes
     */
    public MatriceCarre[] khaletski( Fraction[] secTerme, MatriceCarre[] lesB,
            MatriceCarre[] lesC )
            throws HorsDePorteeException, OperationInvalideException, InversionNulException {
        if ( secTerme.length != this.taille() )
            throw new OperationInvalideException( "La dimension du second terme est de " + secTerme.length
                    + " alors qu'elle doit �tre �gale � " + this.taille() + " pour appliquer la m�thode de khaletski" );

        MatriceCarre b = new MatriceCarre( this.taille() );
        MatriceCarre c = new MatriceCarre( this.taille() );
        for ( int i = 1; i <= this.taille(); i++ )
            c.setCoefficient( i, i, new Fraction( 1 ) );

        // Boucle de remplissage
        for ( int iteration = 1; iteration <= this.taille(); iteration++ ) {
            // Initialisation
            if ( iteration == 1 ) {
                for ( int j = 1; j <= this.taille(); j++ ) {
                    b.setCoefficient( j, 1, this.getCoefficient( j, 1 ) );
                    c.setCoefficient( 1, j, this.getCoefficient( 1, j ).divide(
                            b.getCoefficient( 1, 1 ) ) );
                }
            } else {
                // remplir la ligne de B
                for ( int j = 2; j <= iteration; j++ ) {
                    Fraction sumB = new Fraction( 0 );
                    for ( int k = 1; k < j; k++ )
                        sumB = sumB.add( b.getCoefficient( iteration, k ).multiply(
                                c.getCoefficient( k, j ) ) );

                    b.setCoefficient( iteration, j, this.getCoefficient( iteration, j ).subtract( sumB ) );
                }
                // remplir la colonne de C
                if ( iteration != this.taille() ) {
                    for ( int j = iteration + 1; j <= this.taille(); j++ ) {
                        Fraction sumC = new Fraction( 0 );
                        for ( int k = 1; k < iteration; k++ )
                            sumC = sumC.add( b.getCoefficient( iteration, k ).multiply(
                                    c.getCoefficient( k, j ) ) );
                        c.setCoefficient( iteration, j,
                                b.getCoefficient( iteration, iteration ).inverse().multiply(
                                        this.getCoefficient( iteration, j ).subtract( sumC ) ) );
                    }
                }
            }
            lesB[iteration - 1] = b.clone();
            lesC[iteration - 1] = c.clone();
        }
        // Calcul du d�terminant
        Fraction determinant = new Fraction( 1 );
        for ( int i = 1; i <= b.taille(); i++ )
            determinant = determinant.multiply( b.getCoefficient( i, i ) );

        this.determinant = determinant;

        MatriceCarre[] result = { b, c };
        return result;
    }

    /**
     * Retourne la solution d'une �quation A.X = B o� A est une matrice
     * triangulaire sup�rieure
     * 
     * @param m
     *            Matrice triangulaire sup�rieure
     * @param secTerme
     *            terme B de l'�quation
     * @return Vecteur X solution de l'�quation
     * @throws HorsDePorteeException
     * @see {@link #getCoefficient(int, int)}
     * @throws OperationInvalideException
     *             si secTerme n'est pas un vecteur de m.taille() colonnes ou si
     *             m n'est pas une matrice triangulaire inf�rieure
     */
    public static Fraction[] solveTriangSup( MatriceCarre m, Fraction[] secTerme )
            throws HorsDePorteeException, OperationInvalideException {
        if ( secTerme.length != m.taille() )
            throw new OperationInvalideException(
                    "Le second terme n'est pas un vecteur de taille " + m.taille() );

        for ( int i = 1; i <= m.taille(); i++ )
            for ( int j = 1; j < i; j++ )
                if ( !m.getCoefficient( i, j ).equals( new Fraction( 0 ) ) )
                    throw new OperationInvalideException( "La matrice n'est pas triangulaire sup�rieur.\nCoefficient ("
                            + i + "," + j + ") Diff�rent de 0" );

        Fraction[] result = new Fraction[m.taille()];
        int i = m.taille();
        for ( Fraction f : result ) {
            if ( i == m.taille() )
                result[i - 1] = secTerme[m.taille() - 1].divide( m.getCoefficient( m.taille(), m.taille() ) );
            else {
                Fraction sumLigne = new Fraction( 0 );
                int k;
                for ( k = i + 1; k <= m.taille(); k++ ) {
                    sumLigne = sumLigne
                            .add( m.getCoefficient( i, k ).multiply( result[k - 1] ) );
                }

                result[i - 1] = secTerme[i - 1].subtract( sumLigne ).divide( m.getCoefficient( i, i ) );
            }
            i--;
        }

        return result;
    }

    /**
     * Retourne la solution d'une �quation A.X = B o� A est une matrice
     * triangulaire inf�rieure
     * 
     * @param m
     *            Matrice triangulaire inf�rieure
     * @param secTerme
     *            terme B de l'�quation
     * @return Vecteur X solution de l'�quation
     * @throws HorsDePorteeException
     * @see {@link #getCoefficient(int, int)}
     * @throws OperationInvalideException
     *             si secTerme n'est pas un vecteur de m.taille() colonnes ou si
     *             m n'est pas une matrice triangulaire inf�rieure
     */
    public static Fraction[] solveTriangInf( MatriceCarre m, Fraction[] secTerme )
            throws HorsDePorteeException, OperationInvalideException {
        if ( secTerme.length != m.taille() )
            throw new OperationInvalideException(
                    "Le second terme n'est pas un vecteur de taille " + m.taille() );

        for ( int i = 1; i <= m.taille(); i++ )
            for ( int j = i + 1; j <= m.taille(); j++ )
                if ( !m.getCoefficient( i, j ).equals( new Fraction( 0 ) ) )
                    throw new OperationInvalideException( "La matrice n'est pas triangulaire inf�rieure.\nCoefficient ("
                            + i + "," + j + ") Diff�rent de 0" );

        Fraction[] result = new Fraction[m.taille()];
        int i = 1;
        for ( Fraction f : result ) {
            if ( i == 1 )
                result[i - 1] = secTerme[0].divide( m.getCoefficient( 1, 1 ) );
            else {
                Fraction sumLigne = new Fraction( 0 );
                int k;
                for ( k = 1; k <= i - 1; k++ ) {
                    sumLigne = sumLigne
                            .add( m.getCoefficient( i, k ).multiply( result[k - 1] ) );
                }

                result[i - 1] = secTerme[i - 1].subtract( sumLigne ).divide( m.getCoefficient( i, i ) );
            }
            i++;
        }

        return result;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.deepHashCode( coefficients );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        MatriceCarre other = (MatriceCarre) obj;
        if ( !Arrays.deepEquals( coefficients, other.coefficients ) )
            return false;
        return true;
    }

    /**
     * @return copie profonde de l'objet courant
     */
    public MatriceCarre clone() {
        MatriceCarre clone = new MatriceCarre( this.taille() );
        for ( int i = 1; i <= this.taille(); i++ )
            for ( int j = 1; j <= this.taille(); j++ )
                try {
                    clone.setCoefficient( i, j,
                            new Fraction(
                                    this.getCoefficient( i, j ).getSigne() * this.getCoefficient( i, j ).getNumerator(),
                                    this.getCoefficient( i, j ).getDenominator() ) );
                } catch ( IllegalArgumentException | HorsDePorteeException e ) {
                    e.printStackTrace();
                }

        return clone;

    }

    public String toString() {
        String result = "";
        for ( Fraction[] ligne : this.coefficients ) {
            for ( Fraction coeff : ligne ) {
                result += coeff + " ";
            }
            result += "\n";
        }

        return result;
    }
}
