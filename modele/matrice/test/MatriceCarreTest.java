package modele.matrice.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modele.fraction.Fraction;
import modele.fraction.exception.InversionNulException;
import modele.matrice.MatriceCarre;
import modele.matrice.exception.HorsDePorteeException;
import modele.matrice.exception.IninversibleException;
import modele.matrice.exception.OperationInvalideException;

public class MatriceCarreTest {

    MatriceCarre m;

    @Before
    public void setUp() throws Exception {
        m = new MatriceCarre( 3 );
    }

    @After
    public void tearDown() throws Exception {
        m = null;
    }

    @Test( expected = IllegalArgumentException.class )
    public void testExceptionMatriceDeTailleNegativeOuNul() {
        m = new MatriceCarre( 0 );
    }

    @Test
    public void testTailleMatrice() {
        assertEquals( 3, m.taille() );
    }

    @Test( expected = HorsDePorteeException.class )
    public void testExceptionGetCoeffHorsDePorteeInf() throws HorsDePorteeException {
        m.getCoefficient( 0, 0 );
    }

    @Test( expected = HorsDePorteeException.class )
    public void testExceptionGetCoeffHorsDePorteeSup() throws HorsDePorteeException {
        m.getCoefficient( 4, 4 );
    }

    @Test( expected = HorsDePorteeException.class )
    public void testExceptionSetCoeffHorsDePorteeInf() throws HorsDePorteeException {
        m.setCoefficient( 0, 0, new Fraction( 0 ) );
    }

    @Test( expected = HorsDePorteeException.class )
    public void testExceptionSetCoeffHorsDePorteeSup() throws HorsDePorteeException {
        m.setCoefficient( 4, 4, new Fraction( 0 ) );
    }

    @Test
    public void testSetAndGet() throws HorsDePorteeException {
        m.setCoefficient( 2, 2, new Fraction( 5 ) );
        assertEquals( new Fraction( 5 ), m.getCoefficient( 2, 2 ) );
    }

    @Test
    public void testSetCoefficients() throws Exception {
        m = new MatriceCarre( 2 );
        Fraction[] coefs = { new Fraction( 1 ), new Fraction( 2 ), new Fraction( 3 ), new Fraction( 4 ) };
        m.setCoefficients( coefs );
        MatriceCarre a = new MatriceCarre( 2 );
        a.setCoefficient( 1, 1, new Fraction( 1 ) );
        a.setCoefficient( 1, 2, new Fraction( 2 ) );
        a.setCoefficient( 2, 1, new Fraction( 3 ) );
        a.setCoefficient( 2, 2, new Fraction( 4 ) );
        assertEquals( a, m );
    }

    @Test( expected = OperationInvalideException.class )
    public void testSetCoefficientsException() throws Exception {
        Fraction[] coefs = { new Fraction( 1 ), new Fraction( 2 ), new Fraction( 3 ), new Fraction( 4 ) };
        m.setCoefficients( coefs );

    }

    @Test( expected = OperationInvalideException.class )
    public void testExceptionAdditionnerMatricesTaillesDifferentes() throws Exception {
        MatriceCarre a = new MatriceCarre( 4 );
        MatriceCarre.additionner( a, m );
    }

    @Test
    public void testAdditionner() throws Exception {
        m.setCoefficient( 1, 1, new Fraction( 1 ) );
        m.setCoefficient( 2, 2, new Fraction( 2 ) );

        MatriceCarre a = new MatriceCarre( 3 );
        a.setCoefficient( 1, 3, new Fraction( 2 ) );
        a.setCoefficient( 2, 2, new Fraction( 2 ) );
        a.setCoefficient( 3, 1, new Fraction( 1 ) );

        MatriceCarre b = new MatriceCarre( 3 );
        b.setCoefficient( 1, 1, new Fraction( 1 ) );
        b.setCoefficient( 2, 2, new Fraction( 4 ) );
        b.setCoefficient( 1, 3, new Fraction( 2 ) );
        b.setCoefficient( 3, 1, new Fraction( 1 ) );

        assertEquals( b, MatriceCarre.additionner( m, a ) );
    }

    @Test( expected = OperationInvalideException.class )
    public void testExceptionMultiplierMatricesTaillesDifferentes() throws Exception {
        MatriceCarre a = new MatriceCarre( 4 );
        MatriceCarre.multiplier( a, m );
    }

    @Test
    public void testMultiplier() throws Exception {
        MatriceCarre a = new MatriceCarre( 2 );
        Fraction[] coefA = { new Fraction( 1 ), new Fraction( 2 ), new Fraction( 3 ), new Fraction( 4 ) };
        a.setCoefficients( coefA );

        MatriceCarre b = new MatriceCarre( 2 );
        Fraction[] coefB = { new Fraction( 4 ), new Fraction( 3 ), new Fraction( 2 ), new Fraction( 1 ) };
        b.setCoefficients( coefB );

        MatriceCarre resultat = new MatriceCarre( 2 );
        Fraction[] coefRes = { new Fraction( 8 ), new Fraction( 5 ), new Fraction( 20 ), new Fraction( 13 ) };
        resultat.setCoefficients( coefRes );

        assertEquals( resultat, MatriceCarre.multiplier( a, b ) );
    }

    @Test
    public void testTrace() throws Exception {
        m.setCoefficient( 1, 1, new Fraction( 1 ) );
        m.setCoefficient( 2, 2, new Fraction( 2 ) );
        m.setCoefficient( 3, 3, new Fraction( 3 ) );

        assertEquals( new Fraction( 6 ), m.trace() );
    }

    @Test
    public void testGenererMatriceIdentite() throws Exception {
        m = MatriceCarre.genererMatriceIdentite( 2 );
        MatriceCarre a = new MatriceCarre( 2 );
        a.setCoefficient( 1, 1, new Fraction( 1 ) );
        a.setCoefficient( 2, 2, new Fraction( 1 ) );

        assertEquals( a, m );
    }

    @Test
    public void testmultiplierAVecReel() throws Exception {
        m = new MatriceCarre( 2 );
        Fraction[] coefsM = { new Fraction( 1 ), new Fraction( -2 ), new Fraction( 3 ), new Fraction( -4 ) };
        m.setCoefficients( coefsM );

        m = MatriceCarre.multiplier( m, new Fraction( -3 ) );

        MatriceCarre a = new MatriceCarre( 2 );

        Fraction[] coefsA = { new Fraction( -3 ), new Fraction( 6 ), new Fraction( -9 ), new Fraction( 12 ) };
        a.setCoefficients( coefsA );

        assertEquals( a, m );
    }

    @Test( expected = IninversibleException.class )
    public void testFadeevException() throws Exception {
        Fraction[] coefs = { new Fraction( 1 ), new Fraction( 2 ), new Fraction( 3 ),
                new Fraction( 4 ), new Fraction( 5 ), new Fraction( 6 ), new Fraction( 7 ), new Fraction( 8 ),
                new Fraction( 9 ) };
        m.setCoefficients( coefs );
        m.fadeev();
    }

    @Test
    public void testGetInverseDetPolCarWithFaddeev() throws Exception {
        Fraction[] coefs = { new Fraction( 1 ), new Fraction( -2 ), new Fraction( 1 ),
                new Fraction( 1 ), new Fraction( -1 ), new Fraction( 0 ), new Fraction( 2 ), new Fraction( 3 ),
                new Fraction( -2 ) };
        m.setCoefficients( coefs );
        m.fadeev();
        MatriceCarre result = m.getInverse();

        MatriceCarre modele = new MatriceCarre( 3 );
        Fraction[] coefM = { new Fraction( 2, 3 ), new Fraction( -1, 3 ), new Fraction( 1, 3 ),
                new Fraction( 2, 3 ), new Fraction( -4, 3 ), new Fraction( 1, 3 ), new Fraction( 5, 3 ),
                new Fraction( -7, 3 ),
                new Fraction( 1, 3 ) };
        modele.setCoefficients( coefM );
        Fraction[] reponse = { new Fraction( 1 ), new Fraction( 2 ), new Fraction( -1 ), new Fraction( -3 ) };
        Fraction[] polCar = m.getPolCaracteristique();

        assertEquals( modele, result );
        assertEquals( new Fraction( 3 ), m.getDeterminant() );
        for ( int i = 0; i <= 3; i++ )
            assertEquals( reponse[i], polCar[i] );
    }

    @Test( expected = OperationInvalideException.class )
    public void testSolveTriangInfXMauvaisaDimension() throws HorsDePorteeException, OperationInvalideException {
        Fraction[] secTerme = new Fraction[1];
        MatriceCarre.solveTriangInf( m, secTerme );
    }

    @Test( expected = OperationInvalideException.class )
    public void testSolveTriangInfNonTriangulaire() throws HorsDePorteeException, OperationInvalideException {
        Fraction[] secTerme = new Fraction[3];
        Fraction[] coefs = { new Fraction( 1 ), new Fraction( -2 ), new Fraction( 1 ),
                new Fraction( 1 ), new Fraction( -1 ), new Fraction( 0 ), new Fraction( 2 ), new Fraction( 3 ),
                new Fraction( -2 ) };
        m.setCoefficients( coefs );
        MatriceCarre.solveTriangInf( m, secTerme );
    }

    @Test
    public void testSolveTriangInf() throws HorsDePorteeException, OperationInvalideException {
        Fraction[] secTerme = { new Fraction( 1 ), new Fraction( 4 ), new Fraction( 3 ) };
        Fraction[] coefsM = { new Fraction( 1 ), new Fraction( 0 ), new Fraction( 0 ),
                new Fraction( 2 ), new Fraction( 3 ), new Fraction( 0 ), new Fraction( 4 ), new Fraction( 5 ),
                new Fraction( 6 ) };
        m.setCoefficients( coefsM );

        Fraction[] reponse = { new Fraction( 1 ), new Fraction( 2, 3 ), new Fraction( -13, 18 ) };

        for ( int i = 0; i < 3; i++ )
            assertEquals( reponse[i], MatriceCarre.solveTriangInf( m, secTerme )[i] );
    }

    @Test( expected = OperationInvalideException.class )
    public void testSolveTriangSupXMauvaisaDimension() throws HorsDePorteeException, OperationInvalideException {
        Fraction[] secTerme = new Fraction[1];
        MatriceCarre.solveTriangInf( m, secTerme );
    }

    @Test( expected = OperationInvalideException.class )
    public void testSolveTriangSupNonTriangulaire() throws HorsDePorteeException, OperationInvalideException {
        Fraction[] secTerme = new Fraction[3];
        Fraction[] coefs = { new Fraction( 1 ), new Fraction( -2 ), new Fraction( 1 ),
                new Fraction( 1 ), new Fraction( -1 ), new Fraction( 0 ), new Fraction( 2 ), new Fraction( 3 ),
                new Fraction( -2 ) };
        m.setCoefficients( coefs );
        MatriceCarre.solveTriangInf( m, secTerme );
    }

    @Test
    public void testSolveTriangSup() throws HorsDePorteeException, OperationInvalideException {
        Fraction[] secTerme = { new Fraction( 1 ), new Fraction( 2 ), new Fraction( 6 ) };
        Fraction[] coefsM = { new Fraction( 1 ), new Fraction( 2 ), new Fraction( 3 ),
                new Fraction( 0 ), new Fraction( 4 ), new Fraction( 5 ), new Fraction( 0 ), new Fraction( 0 ),
                new Fraction( 6 ) };
        m.setCoefficients( coefsM );

        Fraction[] reponse = { new Fraction( -1, 2 ), new Fraction( -3, 4 ), new Fraction( 1 ) };

        for ( int i = 0; i < 3; i++ )
            assertEquals( reponse[i], MatriceCarre.solveTriangSup( m, secTerme )[i] );
    }

    @Test( expected = OperationInvalideException.class )
    public void testKhaletskiExceptionSecondtermeInvalide()
            throws HorsDePorteeException, InversionNulException, OperationInvalideException {
        Fraction[] secTerme = new Fraction[1];
        m.khaletski( secTerme );
    }

    @Test
    public void testKhaletski() throws OperationInvalideException, HorsDePorteeException, InversionNulException {
        Fraction[] coefs = { new Fraction( 1 ), new Fraction( 3 ), new Fraction( 4 ),
                new Fraction( 3 ), new Fraction( 5 ), new Fraction( -4 ), new Fraction( 4 ), new Fraction( 7 ),
                new Fraction( -2 ) };
        m.setCoefficients( coefs );

        Fraction[] secTerme = { new Fraction( 50 ), new Fraction( 2 ), new Fraction( 31 ) };

        MatriceCarre[] resultat = m.khaletski( secTerme );

        MatriceCarre b = new MatriceCarre( 3 );
        Fraction[] coefsB = { new Fraction( 1 ), new Fraction( 0 ), new Fraction( 0 ),
                new Fraction( 3 ), new Fraction( -4 ), new Fraction( 0 ), new Fraction( 4 ), new Fraction( -5 ),
                new Fraction( 2 ) };
        b.setCoefficients( coefsB );
        MatriceCarre c = new MatriceCarre( 3 );
        Fraction[] coefsC = { new Fraction( 1 ), new Fraction( 3 ), new Fraction( 4 ),
                new Fraction( 0 ), new Fraction( 1 ), new Fraction( 4 ), new Fraction( 0 ), new Fraction( 0 ),
                new Fraction( 1 ) };
        c.setCoefficients( coefsC );

        assertEquals( b, resultat[0] );
        assertEquals( c, resultat[1] );

    }

    @Test
    public void testClone() throws OperationInvalideException, HorsDePorteeException, CloneNotSupportedException {
        Fraction[] coefsM = { new Fraction( 1 ), new Fraction( 2 ), new Fraction( 3 ),
                new Fraction( 0 ), new Fraction( 4 ), new Fraction( 5 ), new Fraction( 0 ), new Fraction( 0 ),
                new Fraction( 6 ) };
        m.setCoefficients( coefsM );

        MatriceCarre clone = (MatriceCarre) m.clone();
        assertTrue( m != clone );
        assertTrue( clone.getClass() == m.getClass() );
        assertTrue( clone.equals( m ) );

    }

    @Test( expected = NullPointerException.class )
    public void testGetDeterminantExceptionOnbuild() throws OperationInvalideException {
        m.getDeterminant();
    }

    @Test
    public void testGetDeterminantAfterKhaletski()
            throws OperationInvalideException, HorsDePorteeException, InversionNulException {
        Fraction[] coefs = { new Fraction( 1 ), new Fraction( 3 ), new Fraction( 4 ),
                new Fraction( 3 ), new Fraction( 5 ), new Fraction( -4 ), new Fraction( 4 ), new Fraction( 7 ),
                new Fraction( -2 ) };
        m.setCoefficients( coefs );
        Fraction[] secTerme = { new Fraction( 50 ), new Fraction( 2 ), new Fraction( 31 ) };
        m.khaletski( secTerme );
        assertEquals( new Fraction( -8 ), m.getDeterminant() );
    }

    @Test
    public void testGetDeterminantAfterFadeev()
            throws OperationInvalideException, IninversibleException, HorsDePorteeException {
        Fraction[] coefs = { new Fraction( 1 ), new Fraction( -2 ), new Fraction( 1 ),
                new Fraction( 1 ), new Fraction( -1 ), new Fraction( 0 ), new Fraction( 2 ), new Fraction( 3 ),
                new Fraction( -2 ) };
        m.setCoefficients( coefs );
        m.fadeev();
        assertEquals( new Fraction( 3 ), m.getDeterminant() );
    }

    @Test( expected = NullPointerException.class )
    public void testGetDeterminantExceptionAfterSetCoeff()
            throws OperationInvalideException, HorsDePorteeException, IninversibleException {
        Fraction[] coefs = { new Fraction( 1 ), new Fraction( -2 ), new Fraction( 1 ),
                new Fraction( 1 ), new Fraction( -1 ), new Fraction( 0 ), new Fraction( 2 ), new Fraction( 3 ),
                new Fraction( -2 ) };
        m.setCoefficients( coefs );
        m.fadeev();
        assertEquals( new Fraction( 3 ), m.getDeterminant() );
        m.setCoefficient( 1, 1, new Fraction( 2 ) );
        m.getDeterminant();
    }

    @Test
    public void testMultiplierMatriceVecteur() throws OperationInvalideException, HorsDePorteeException {
        Fraction[] coefs = { new Fraction( 1 ), new Fraction( 2 ), new Fraction( 3 ),
                new Fraction( 1 ), new Fraction( 2 ), new Fraction( 3 ), new Fraction( 1 ), new Fraction( 2 ),
                new Fraction( 3 ) };
        m.setCoefficients( coefs );
        Fraction[] x = { new Fraction( 1 ), new Fraction( 1 ), new Fraction( 1 ) };

        Fraction[] reponse = { new Fraction( 6 ), new Fraction( 6 ), new Fraction( 6 ) };
        Fraction[] resultat = MatriceCarre.multiplier( m, x );

        for ( int i = 0; i < 3; i++ )
            assertEquals( reponse[i], resultat[i] );
    }

    @Test( expected = OperationInvalideException.class )
    public void testMultiplierMatriceVecteurException() throws OperationInvalideException, HorsDePorteeException {

        Fraction[] x = { new Fraction( 1 ), new Fraction( 1 ) };
        MatriceCarre.multiplier( m, x );

    }

    @Test( expected = NullPointerException.class )
    public void testGetInverseExceptionAfterChange() throws Exception {
        Fraction[] coefs = { new Fraction( 1 ), new Fraction( -2 ), new Fraction( 1 ),
                new Fraction( 1 ), new Fraction( -1 ), new Fraction( 0 ), new Fraction( 2 ), new Fraction( 3 ),
                new Fraction( -2 ) };
        m.setCoefficients( coefs );
        m.fadeev();

        m.setCoefficient( 1, 1, new Fraction( 2 ) );
        m.getInverse();
    }

    @Test
    public void testGetPolCar() throws Exception {
        Fraction[] coefs = { new Fraction( 2 ), new Fraction( 2 ), new Fraction( 3 ),
                new Fraction( 4 ), new Fraction( 5 ), new Fraction( 6 ), new Fraction( 7 ), new Fraction( 8 ),
                new Fraction( 9 ) };
        m.setCoefficients( coefs );
        m.fadeev();

        Fraction[] reponse = { new Fraction( 1 ), new Fraction( -16 ), new Fraction( -4 ), new Fraction( 3 ) };
        Fraction[] polCar = m.getPolCaracteristique();
        for ( int i = 0; i <= 3; i++ )
            assertEquals( reponse[i], polCar[i] );
    }

    @Test( expected = NullPointerException.class )
    public void testGetPolCarExceptionAfterChange() throws Exception {
        Fraction[] coefs = { new Fraction( 2 ), new Fraction( 2 ), new Fraction( 3 ),
                new Fraction( 4 ), new Fraction( 5 ), new Fraction( 6 ), new Fraction( 7 ), new Fraction( 8 ),
                new Fraction( 9 ) };
        m.setCoefficients( coefs );
        m.fadeev();

        m.setCoefficient( 1, 1, new Fraction( 0 ) );
        m.getPolCaracteristique();

    }

}
