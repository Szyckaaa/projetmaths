package modele.matrice.exception;

public class HorsDePorteeException extends Exception implements MatriceCarreException {

	public HorsDePorteeException(int ligne, int colonne, int taille) {
		super("\nTentative d'acc�s au coefficient (" + ligne + "," + colonne + ")"
				+ "\nLes coefficients de la matrice vont de 1 � " + taille);
	}

}
