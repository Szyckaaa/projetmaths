package modele.matrice.exception;

public class IninversibleException extends Exception implements MatriceCarreException {
    public IninversibleException() {
        super( "La matrice n'est pas inversible (son déterminant est nul)" );
    }
}
