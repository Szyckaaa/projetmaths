package modele.matrice.exception;

public class OperationInvalideException extends Exception implements MatriceCarreException {
	public OperationInvalideException(String message) {
		super("Opération invalide : " + message);
	}
}
