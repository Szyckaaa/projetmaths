package modele;

import modele.fraction.Fraction;
import modele.matrice.MatriceCarre;
import modele.matrice.exception.HorsDePorteeException;
import modele.matrice.exception.IninversibleException;
import modele.matrice.exception.OperationInvalideException;

/**
 * Ce point d'entr�e Java illustre le fonctionnement de l'algorithme de Fadeev
 * 
 * @author Julien (Huss Huss)
 *
 */
public class fadeev {

    public static void main( String[] args ) throws HorsDePorteeException, OperationInvalideException {
        exampleForApp();
    }

    /**
     * Cette m�thode illustre l'algorithme � appliquer dans l'application pour
     * notre projet, avec pour exemple la matrice B du sujet de projet
     * 
     * @throws HorsDePorteeException
     * @throws OperationInvalideException
     */
    public static void exampleForApp() throws OperationInvalideException, HorsDePorteeException {
        /*
         * L'objectif est de r�soudre une �quation de la forme A . X = B o� A
         * est une matrice de taille n, X un vecteur inconnue de n ligne et B un
         * vecteur connue de taille n
         * 
         * On peut manipuler l'�quation pour obtenir l'expression de X : X =
         * A^-1 . B
         */

        // La premi�re �tape consiste � d�clarer A et l'initialiser
        MatriceCarre A = new MatriceCarre( 4 );
        Fraction[] coefs = { new Fraction( 3 ), new Fraction( 1 ), new Fraction( -1 ),
                new Fraction( 2 ), new Fraction( -5 ), new Fraction( 1 ), new Fraction( 3 ), new Fraction( -4 ),
                new Fraction( 2 ), new Fraction( 0 ), new Fraction( 1 ), new Fraction( -1 ), new Fraction( 1 ),
                new Fraction( -5 ), new Fraction( 3 ), new Fraction( -3 ) };
        A.setCoefficients( coefs );
        System.out.println( "Matrice A :\n" + A );

        // De A on d�duit la taille de B
        Fraction[] B = new Fraction[A.taille()];
        // on rempli B
        B[0] = new Fraction( 1 );
        B[1] = new Fraction( 2 );
        B[2] = new Fraction( 3 );
        B[3] = new Fraction( 4 );

        int i = 1;
        for ( Fraction f : B ) {
            System.out.println( "B" + i + ":" + f );
            i++;
        }

        // On applique Faddeev sur la matrice A qui peut renvoyer une exception
        // si elle n'est pas inversible
        try {
            A.fadeev();
        } catch ( IninversibleException e ) {
            e.printStackTrace();
        }

        /*
         * Faddeev initialise alors diff�rents attributs sur la matrice : -
         * Determinant - Inverse -Polynome caract�ristique
         */
        System.out.println( "\nD�terminant de A : " + A.getDeterminant() );
        System.out.print( "Polyn�me caract�ristique de A : " );
        i = A.taille();
        for ( Fraction f : A.getPolCaracteristique() ) {
            System.out.print( f + "x^" + i + " + " );
            i--;
        }

        System.out.println( "\nInverse de A :\n" + A.getInverse() );

        // On peut alors r�soudre notre �quation de d�part
        Fraction[] X = MatriceCarre.multiplier( A.getInverse(), B );

        i = 1;
        for ( Fraction f : X )
            System.out.println( "X" + i + ":" + f );

        // Et v�rifier qu'elle est correct
        Fraction[] check = MatriceCarre.multiplier( A, X );

        System.out.println( "\nV�rification, r�sultat de A.X :" );
        i = 1;
        for ( Fraction f : check ) {
            System.out.println( "B" + i + ":" + f );
            i++;
        }

    }

}
