package vues;

import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JTextField;

import modele.fraction.Fraction;

public class VueVecteur extends JPanel {

    // Attributes
    private int          taille;
    private JTextField[] composantes;

    // Constructor
    public VueVecteur( int taille ) {
        this.composantes = new JTextField[taille];
        this.taille = taille;
        this.setLayout( new GridLayout( this.taille, 1 ) );
        for ( int i = 0; i < taille; i++ ) {
            JTextField txt = new JTextField( "" );
            this.composantes[i] = txt;
            Font fontTxt = new Font( "Arial", Font.BOLD, 18 );
            txt.setFont( fontTxt );
            txt.setHorizontalAlignment( JTextField.CENTER );
            this.add( new JPanel().add( txt ) );
        }
    }

    public JTextField getComposante( int i ) {
        return this.composantes[i - 1];
    }

    public void setComposante( int i, Fraction valeur ) {
        this.composantes[i - 1].setText( valeur.toString( false ) );
        this.composantes[i - 1].setEditable( false );
    }

}
