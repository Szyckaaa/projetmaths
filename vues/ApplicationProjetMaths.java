package vues;

import javax.swing.JFrame;

import modele.fraction.Fraction;
import modele.matrice.exception.HorsDePorteeException;
import modele.matrice.exception.OperationInvalideException;

public class ApplicationProjetMaths {
	
	public static void main(String[] args) throws OperationInvalideException, HorsDePorteeException {
		
	    JFrame f = new JFrame("Faddeev et Khaletski");
	    f.setResizable(false);
	    f.setSize(800, 800);
	    VueProjetMaths vue = new VueProjetMaths();
	    f.getContentPane().add(vue);
	    f.setVisible(true);
	    
	  }
}