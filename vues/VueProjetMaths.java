package vues;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controleur.ControleurProjetMaths;
import modele.matrice.exception.HorsDePorteeException;
import modele.matrice.exception.OperationInvalideException;

public class VueProjetMaths extends JPanel {

	// Attributes
	GridBagConstraints gbc;
	// Groupe d'onglets de l'application
	JTabbedPane onglets; 
	// Le controleur permettant de naviguer et d'effectuer les op�rations
	ControleurProjetMaths controleur; 
	// Champs textuel contenant la taille de la matrice souhait�e pour la saisie des donn�es
	JTextField valeurTailleMatrice; 
	// Panneau contenant la matrice et le vecteurs g�n�r�s selon la taille souhait�e
	JPanel encartMilieuSaisie; 
	// Panneau contenant les boutons de navigation vers la M�thode de Faddeev ou Khaletski
	JPanel encartBasSaisie; 
	// Panneau contenant la matrice A dans l'onglet "Saisie des donn�es"	
	JPanel cadreMatriceA; 
	// Panneau contenant le vecteur B dans l'onglet "Saisie des donn�es"
	JPanel cadreVecteurB; 	
	// Panneau contenant le r�sultat de l'application de la m�thode de Faddeev
	JPanel encartCentreFaddeev; 
	// Panneau contenant la matrice inverse g�n�r�e apr�s l'application de la m�thode de Faddeev
	JPanel cadreMatriceInverse; 
	// Panneau contenant la matrice B g�n�r�e apr�s l'application de la m�thode de Faddeev
	JPanel cadreMatriceBF; 
	// Champs de texte contenant le d�terminant g�n�r� apr�s l'application de la m�thode de Faddeev
	JTextField determinantF; 
	// Champs de texte contenant le polyn�me caract�ristique g�n�r� apr�s l'application de la m�thode de Faddeev
	JTextField polynome; 
	// Panneau contenant les diff�rents �l�ments g�n�r�s suite � la m�thode de Khaletski
	JPanel encartCentreKhaletski; 
	// Panneau contenant la matrice B g�n�r�e apr�s l'application de la m�thode de	Khaletski	
	JPanel cadreMatriceBK; 
	// Panneau contenant la matrice C g�n�r�e apr�s l'application de la m�thode de Khaletski
	JPanel cadreMatriceCK; 
	// Champs de texte contenant le d�terminant g�n�r� apr�s l'application de la m�thode de Khaletski
	JTextField determinantK; 
	// Panneau contenant les diff�rents �l�ments g�n�r�s suite � l'application d'une des deux m�thodes
	JPanel encartCentreResultats; 
	// Panneau contenant la matrice A g�n�r�e suite l'application d'une des deux m�thodes
	JPanel cadreMatriceAR; 
	// Panneau contenant le vecteur X g�n�r� suite � l'application d'une des deux m�thodes
	JPanel cadreVecteurX; 
	// Panneau contenant le vecteur B g�n�r� suite � l'application d'une des deux m�thodes
	JPanel cadreVecteurBR; 
	
	JTextArea infosResultats;
		
	GridBagConstraints gbcMatrice;

	Color songeEte = new Color(180, 208, 217);
	Color pecheHiver = new Color(235,217,208);

	// Constructor
	public VueProjetMaths() throws OperationInvalideException, HorsDePorteeException {

		// Instanciation du controleur
		this.controleur = new ControleurProjetMaths(this);

		this.gbc = new GridBagConstraints();
		this.gbcMatrice = new GridBagConstraints();
		this.gbcMatrice.insets = new Insets(0, 5, 0, 5);

		// Ajout des onglets
		this.onglets = new JTabbedPane(SwingConstants.TOP);
		this.add(this.onglets);
		this.onglets.setOpaque(true);

		// Onglet : Accueil (1)
		// ----------------------------------------------------------------
		JPanel ongletAccueil = new JPanel();
		ongletAccueil.setPreferredSize(new Dimension(750, 750));
		ongletAccueil.setLayout(new GridBagLayout());
		ongletAccueil.setBackground(new Color(250, 241, 231));
		this.onglets.addTab("Accueil", ongletAccueil);

		// Accueil : Encart titre
		JPanel titreAccueil = new JPanel();
		titreAccueil.setPreferredSize(new Dimension(750, 100));
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		titreAccueil.setBackground(songeEte);
		ongletAccueil.add(titreAccueil, this.gbc);
		titreAccueil.setLayout(new GridBagLayout());

		JLabel titre1 = new JLabel("M�thode de Faddeev");
		Font font_titre1 = new Font("Arial", Font.BOLD, 26);
		titre1.setFont(font_titre1);
		titre1.setHorizontalAlignment(SwingConstants.CENTER);
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		titreAccueil.add(titre1, this.gbc);

		JLabel titre2 = new JLabel("M�thode de Khaletski");
		Font font_titre2 = new Font("Arial", Font.BOLD, 26);
		titre2.setFont(font_titre2);
		titre2.setHorizontalAlignment(SwingConstants.CENTER);
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		titreAccueil.add(titre2, this.gbc);

		// Accueil : Panneau central
		JPanel centreAccueil = new JPanel();
		centreAccueil.setPreferredSize(new Dimension(750, 550));
		centreAccueil.setBackground(pecheHiver);
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		ongletAccueil.add(centreAccueil, this.gbc);
		centreAccueil.setLayout(new GridBagLayout());

		ImageIcon imageFaddeev = new ImageIcon(
				"C:\\Users\\Valentin\\eclipse-workspace\\ProjetMaths\\src\\ressources\\faddeev.jpg");
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		centreAccueil.add(new JLabel(imageFaddeev), this.gbc);

		JLabel etiquetteImage = new JLabel("Dmitry Faddeev");
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		etiquetteImage.setHorizontalAlignment(SwingConstants.CENTER);
		centreAccueil.add(etiquetteImage, this.gbc);

		JButton cestParti = new JButton("C'est parti !");
		this.gbc.gridx = 0;
		this.gbc.gridy = 2;
		cestParti.addActionListener(this.controleur);
		centreAccueil.add(cestParti, this.gbc);

		// Accueil : Pied de page
		JPanel piedPage = new JPanel();
		piedPage.setPreferredSize(new Dimension(750, 100));
		piedPage.setBackground(songeEte);
		ongletAccueil.add(piedPage, this.gbc);
		this.gbc.gridx = 0;
		this.gbc.gridy = 2;
		piedPage.setLayout(new GridBagLayout());

		JLabel createurs = new JLabel("Valentin LAPORTE - Julien WATTIER - Gatien XUEREB");
		createurs.setHorizontalAlignment(SwingConstants.CENTER);
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		piedPage.add(createurs, this.gbc);

		JLabel referent = new JLabel("R�f�rent : Abdel-Kaddous TAHA");
		referent.setHorizontalAlignment(SwingConstants.CENTER);
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		piedPage.add(referent, this.gbc);

		// Onglet : Saisie des donn�es
		JPanel ongletSaisie = new JPanel();
		ongletSaisie.setPreferredSize(new Dimension(750, 750));
		ongletSaisie.setLayout(new GridBagLayout());
		ongletSaisie.setBackground(new Color(250, 241, 231));
		this.onglets.addTab("Saisies des donn�es", ongletSaisie);

		// Saisie : Encart haut
		JPanel encartHautSaisie = new JPanel();
		encartHautSaisie.setPreferredSize(new Dimension(750, 100));
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartHautSaisie.setBackground(songeEte);
		ongletSaisie.add(encartHautSaisie, this.gbc);
		encartHautSaisie.setLayout(new GridBagLayout());

		JLabel titreSaisie = new JLabel("Saisies des donn�es");
		Font font_titreSaisie = new Font("Arial", Font.BOLD, 26);
		titreSaisie.setFont(font_titreSaisie);
		titreSaisie.setPreferredSize(new Dimension(750, 50));
		titreSaisie.setHorizontalAlignment(SwingConstants.CENTER);
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartHautSaisie.add(titreSaisie, this.gbc);

		JPanel saisieTailleMatrice = new JPanel();
		saisieTailleMatrice.setPreferredSize(new Dimension(750, 50));
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		saisieTailleMatrice.setBackground(songeEte);
		encartHautSaisie.add(saisieTailleMatrice, this.gbc);
		saisieTailleMatrice.setLayout(new GridBagLayout());

		JLabel tailleMatrice = new JLabel("Taille de la matrice :");
		Font font_tailleMatrice = new Font("Arial", Font.BOLD, 18);
		tailleMatrice.setFont(font_tailleMatrice);
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		saisieTailleMatrice.add(tailleMatrice, this.gbc);

		this.valeurTailleMatrice = new JTextField();
		this.valeurTailleMatrice.setPreferredSize(new Dimension(100, 30));
		this.valeurTailleMatrice.setFont(font_tailleMatrice);
		this.valeurTailleMatrice.setHorizontalAlignment( JTextField.CENTER );
		GridBagConstraints gbc_valeurTailleMatrice = new GridBagConstraints();
		gbc_valeurTailleMatrice.insets = new Insets(0,20,0,20);
		gbc_valeurTailleMatrice.gridx = 1;
		gbc_valeurTailleMatrice.gridy = 0;
		saisieTailleMatrice.add(this.valeurTailleMatrice, gbc_valeurTailleMatrice);

		JButton genererMatrice = new JButton("G�n�rer");
		genererMatrice.setPreferredSize(new Dimension(200,30));
		genererMatrice.setFont(font_tailleMatrice);
		this.gbc.gridx = 2;
		this.gbc.gridy = 0;
		genererMatrice.addActionListener(this.controleur);
		saisieTailleMatrice.add(genererMatrice, this.gbc);

		// Saisie : Encart milieu
		JPanel encartG�n�ration = new JPanel();
		encartG�n�ration.setPreferredSize(new Dimension(750, 550));
		encartG�n�ration.setBackground(pecheHiver);
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		ongletSaisie.add(encartG�n�ration, this.gbc);
		encartG�n�ration.setLayout(new GridBagLayout());

		this.encartMilieuSaisie = new JPanel();
		this.encartMilieuSaisie.setPreferredSize(new Dimension(750, 550));
		this.encartMilieuSaisie.setBackground(pecheHiver);
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		encartG�n�ration.add(this.encartMilieuSaisie, this.gbc);
		this.encartMilieuSaisie.setLayout(new GridBagLayout());
		this.encartMilieuSaisie.setVisible(false);

		// Partie Matrice A
		JPanel encartMatriceA = new JPanel();
		encartMatriceA.setPreferredSize(new Dimension(365, 550));
		encartMatriceA.setBackground(pecheHiver);
		this.gbcMatrice.gridx = 0;
		this.gbcMatrice.gridy = 0;
		this.encartMilieuSaisie.add(encartMatriceA, this.gbcMatrice);
		encartMatriceA.setLayout(new GridBagLayout());

		JLabel labelMatriceA = new JLabel("Matrice A :");
		Font font_labelMatriceA = new Font("Arial", Font.BOLD, 20);
		labelMatriceA.setFont(font_labelMatriceA);
		labelMatriceA.setPreferredSize(new Dimension(365, 100));
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartMatriceA.add(labelMatriceA, this.gbc);

		this.cadreMatriceA = new JPanel();
		this.cadreMatriceA.setPreferredSize(new Dimension(365, 450));
		this.cadreMatriceA.setLayout(new GridLayout(1, 1));
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		encartMatriceA.add(this.cadreMatriceA, this.gbc);

		// Partie Vecteur B
		JPanel encartVecteurB = new JPanel();
		encartVecteurB.setPreferredSize(new Dimension(365, 550));
		encartVecteurB.setBackground(pecheHiver);
		this.gbcMatrice.gridx = 1;
		this.gbcMatrice.gridy = 0;
		this.encartMilieuSaisie.add(encartVecteurB, this.gbcMatrice);
		encartVecteurB.setLayout(new GridBagLayout());

		JLabel labelVecteurB = new JLabel("Vecteur B :");
		Font font_labelVecteurB = new Font("Arial", Font.BOLD, 20);
		labelVecteurB.setFont(font_labelVecteurB);
		labelVecteurB.setPreferredSize(new Dimension(365, 100));
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartVecteurB.add(labelVecteurB, this.gbc);

		this.cadreVecteurB = new JPanel();
		this.cadreVecteurB.setPreferredSize(new Dimension(365, 450));
		this.cadreVecteurB.setLayout(new GridLayout(1, 1));
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		encartVecteurB.add(this.cadreVecteurB, this.gbc);

		// Saisie : Encart bas
		JPanel encartG�n�rationBas = new JPanel();
		encartG�n�rationBas.setPreferredSize(new Dimension(750, 100));
		encartG�n�rationBas.setBackground(pecheHiver);
		this.gbc.gridx = 0;
		this.gbc.gridy = 2;
		ongletSaisie.add(encartG�n�rationBas, this.gbc);
		encartG�n�rationBas.setLayout(new GridBagLayout());

		this.encartBasSaisie = new JPanel();
		this.encartBasSaisie.setPreferredSize(new Dimension(750, 100));
		this.encartBasSaisie.setBackground(pecheHiver);
		encartG�n�rationBas.add(this.encartBasSaisie, this.gbc);
		this.gbc.gridx = 0;
		this.gbc.gridy = 2;
		this.encartBasSaisie.setLayout(new GridBagLayout());
		this.encartBasSaisie.setVisible(false);

		JButton b_methodeFaddeev = new JButton("M�thode de Faddeev");
		b_methodeFaddeev.setPreferredSize(new Dimension(200, 40));
		GridBagConstraints gbc_b_methode = new GridBagConstraints();
		gbc_b_methode.gridx = 0;
		gbc_b_methode.gridy = 0;
		gbc_b_methode.insets = new Insets(0,10,0,10);
		this.encartBasSaisie.add(b_methodeFaddeev, gbc_b_methode);
		b_methodeFaddeev.addActionListener(this.controleur);
		b_methodeFaddeev.setVisible(true);

		JButton b_methodeKhaletski = new JButton("M�thode de Khaletski");
		b_methodeKhaletski.setPreferredSize(new Dimension(200, 40));
		b_methodeKhaletski.repaint();
		gbc_b_methode.gridx = 1;
		gbc_b_methode.gridy = 0;
		this.encartBasSaisie.add(b_methodeKhaletski, gbc_b_methode);
		b_methodeKhaletski.addActionListener(this.controleur);

		// Onglet : M�thode de Faddeev
		JPanel ongletFaddeev = new JPanel();
		ongletFaddeev.setPreferredSize(new Dimension(750, 750));
		ongletFaddeev.setLayout(new GridBagLayout());
		ongletFaddeev.setBackground(new Color(250, 241, 231));
		this.onglets.addTab("M�thode de Faddeev", ongletFaddeev);

		// M�thode de Faddeev : Encart haut
		JPanel encartHautFaddeev = new JPanel();
		encartHautFaddeev.setPreferredSize(new Dimension(750, 200));
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartHautFaddeev.setBackground(songeEte);
		ongletFaddeev.add(encartHautFaddeev, this.gbc);
		encartHautFaddeev.setLayout(new GridBagLayout());

		JLabel titreFaddeev = new JLabel("M�thode de Faddeev");
		Font font_titreFaddeev = new Font("Arial", Font.BOLD, 26);
		titreFaddeev.setFont(font_titreFaddeev);
		titreFaddeev.setPreferredSize(new Dimension(750, 50));
		titreFaddeev.setHorizontalAlignment(SwingConstants.CENTER);
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartHautFaddeev.add(titreFaddeev, this.gbc);

		JPanel traitementFaddeev = new JPanel();
		traitementFaddeev.setPreferredSize(new Dimension(750, 150));
		traitementFaddeev.setBackground(pecheHiver);
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		encartHautFaddeev.add(traitementFaddeev, this.gbc);
		traitementFaddeev.setLayout(new GridBagLayout());

		JTextArea infosFaddeev = new JTextArea("La m�thode consiste � appliquer un proc�d� it�ratif "
				+ "permettant de" +"\n" +"d�terminer rapidemment le polyn�me caract�ristique "
				+ "d'une" + "\n" + "matrice, mais �galement son d�terminant et son inverse dans" + "\n" + "la foul�e.");
		infosFaddeev.setLineWrap(true);
		Font font_infosFaddeev = new Font("Arial", Font.BOLD, 16);
		infosFaddeev.setFont(font_infosFaddeev);
		infosFaddeev.setPreferredSize(new Dimension(500, 110));
		infosFaddeev.setBackground(pecheHiver);
		GridBagConstraints gbc_infosFaddeev = new GridBagConstraints();
		gbc_infosFaddeev.insets = new Insets(0, 20, 0, 20);
		gbc_infosFaddeev.gridx = 0;
		gbc_infosFaddeev.gridy = 0;
		gbc_infosFaddeev.gridheight = 2;
		traitementFaddeev.add(infosFaddeev, gbc_infosFaddeev);

		JButton lancerFaddeev = new JButton("Lancer le traitement");
		lancerFaddeev.setPreferredSize(new Dimension(150, 40));
		this.gbc.gridx = 1;
		this.gbc.gridy = 0;
		lancerFaddeev.addActionListener(this.controleur);
		traitementFaddeev.add(lancerFaddeev, this.gbc);

		JButton allerResultatsF = new JButton("Voir r�sultat");
		allerResultatsF.setPreferredSize(new Dimension(150, 40));
		this.gbc.gridx = 1;
		this.gbc.gridy = 1;
		allerResultatsF.addActionListener(this.controleur);
		traitementFaddeev.add(allerResultatsF, this.gbc);

		// M�thode de Faddeev : Encart centre
		JPanel encartG�n�rationFaddeev = new JPanel();
		encartG�n�rationFaddeev.setPreferredSize(new Dimension(750, 550));
		encartG�n�rationFaddeev.setBackground(pecheHiver);
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		ongletFaddeev.add(encartG�n�rationFaddeev, this.gbc);
		encartG�n�rationFaddeev.setLayout(new GridBagLayout());

		this.encartCentreFaddeev = new JPanel();
		this.encartCentreFaddeev.setPreferredSize(new Dimension(750, 550));
		this.encartCentreFaddeev.setBackground(pecheHiver);
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartG�n�rationFaddeev.add(this.encartCentreFaddeev, this.gbc);
		this.encartCentreFaddeev.setLayout(new GridBagLayout());
		this.encartCentreFaddeev.setVisible(false);

		JPanel encartMatriceInverse = new JPanel();
		encartMatriceInverse.setPreferredSize(new Dimension(425, 500));
		encartMatriceInverse.setBackground(pecheHiver);
		this.gbcMatrice.gridx = 0;
		this.gbcMatrice.gridy = 0;
		this.encartCentreFaddeev.add(encartMatriceInverse, this.gbcMatrice);
		encartMatriceInverse.setLayout(new GridBagLayout());

		JLabel labelMatriceInverse = new JLabel("Matrice inverse :");
		Font font_labelMatriceInverse = new Font("Arial", Font.BOLD, 20);
		labelMatriceInverse.setFont(font_labelMatriceInverse);
		labelMatriceInverse.setPreferredSize(new Dimension(425, 50));
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartMatriceInverse.add(labelMatriceInverse, this.gbc);

		this.cadreMatriceInverse = new JPanel();
		this.cadreMatriceInverse.setPreferredSize(new Dimension(425, 450));
		this.cadreMatriceInverse.setLayout(new GridLayout(1, 1));
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		encartMatriceInverse.add(this.cadreMatriceInverse, this.gbc);

		JPanel encartDetPolynome = new JPanel();
		encartDetPolynome.setPreferredSize(new Dimension(250, 500));
		encartDetPolynome.setBackground(pecheHiver);
		this.gbcMatrice.gridx = 1;
		this.gbcMatrice.gridy = 0;
		this.encartCentreFaddeev.add(encartDetPolynome, this.gbcMatrice);
		encartDetPolynome.setLayout(new GridBagLayout());

		JLabel labelDeterminantF = new JLabel("D�terminant :");
		Font font_labelDeterminantF = new Font("Arial", Font.BOLD, 16);
		labelDeterminantF.setFont(font_labelDeterminantF);
		labelDeterminantF.setPreferredSize(new Dimension(250, 50));
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartDetPolynome.add(labelDeterminantF, this.gbc);

		this.determinantF = new JTextField();
		this.determinantF.setPreferredSize(new Dimension(250, 50));
		Font font_determinantF = new Font("Arial", Font.BOLD, 16);
		this.determinantF.setFont(font_determinantF);
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		encartDetPolynome.add(this.determinantF, this.gbc);

		JLabel labelPolynome = new JLabel("Polyn�me caract�ristique :");
		Font font_labelPolynome = new Font("Arial", Font.BOLD, 16);
		labelPolynome.setFont(font_labelPolynome);
		labelPolynome.setPreferredSize(new Dimension(250, 50));
		this.gbc.gridx = 0;
		this.gbc.gridy = 2;
		encartDetPolynome.add(labelPolynome, this.gbc);

		this.polynome = new JTextField();
		this.polynome.setPreferredSize(new Dimension(250, 50));
		Font font_polynome = new Font("Arial", Font.BOLD, 16);
		this.polynome.setFont(font_polynome);
		this.gbc.gridx = 0;
		this.gbc.gridy = 3;
		encartDetPolynome.add(this.polynome, this.gbc);

		JButton retourSaisieF = new JButton("Retour � la saisie");
		GridBagConstraints gbc_retourSaisieF = new GridBagConstraints();
		Font font_retourSaisieF = new Font("Arial", Font.BOLD, 16);
		retourSaisieF.setFont(font_retourSaisieF);
		gbc_retourSaisieF.insets = new Insets(20, 0, 20, 0);
		retourSaisieF.setPreferredSize(new Dimension(250, 50));
		gbc_retourSaisieF.gridx = 0;
		gbc_retourSaisieF.gridy = 4;
		retourSaisieF.addActionListener(this.controleur);
		encartDetPolynome.add(retourSaisieF, gbc_retourSaisieF);

		// Onglet : M�thode de Khaletski
		JPanel ongletKhaletski = new JPanel();
		ongletKhaletski.setPreferredSize(new Dimension(750, 750));
		ongletKhaletski.setLayout(new GridBagLayout());
		ongletKhaletski.setBackground(new Color(250, 241, 231));
		this.onglets.addTab("M�thode de Khaletski", ongletKhaletski);

		// M�thode de Khaletski : Encart haut
		JPanel encartHautKhaletski = new JPanel();
		encartHautKhaletski.setPreferredSize(new Dimension(750, 200));
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartHautKhaletski.setBackground(songeEte);
		ongletKhaletski.add(encartHautKhaletski, this.gbc);
		encartHautKhaletski.setLayout(new GridBagLayout());

		JLabel titreKhaletski = new JLabel("M�thode de Khaletski");
		Font font_titreKhaletski = new Font("Arial", Font.BOLD, 26);
		titreKhaletski.setFont(font_titreKhaletski);
		titreKhaletski.setPreferredSize(new Dimension(750, 50));
		titreKhaletski.setHorizontalAlignment(SwingConstants.CENTER);
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartHautKhaletski.add(titreKhaletski, this.gbc);

		JPanel traitementKhaletski = new JPanel();
		traitementKhaletski.setPreferredSize(new Dimension(750, 150));
		traitementKhaletski.setBackground(pecheHiver);
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		encartHautKhaletski.add(traitementKhaletski, this.gbc);
		traitementKhaletski.setLayout(new GridBagLayout());

		JTextArea infosKhaletski = new JTextArea("La m�thode consiste � appliquer une d�composition LU "
				+ "sur la" + "\n" + "matrice, afin de r�soudre rapidemment une �quation de type" + "\n" + "A.x = b . "
				+ "On peut �galement obtenir son d�terminant au passage.");
		infosKhaletski.setLineWrap(true);
		Font font_infosKhaletski = new Font("Arial", Font.BOLD, 16);
		infosKhaletski.setFont(font_infosKhaletski);
		infosKhaletski.setPreferredSize(new Dimension(500, 110));
		infosKhaletski.setBackground(pecheHiver);
		GridBagConstraints gbc_infosKhaletski = new GridBagConstraints();
		gbc_infosKhaletski.insets = new Insets(0, 20, 0, 20);
		gbc_infosKhaletski.gridx = 0;
		gbc_infosKhaletski.gridy = 0;
		gbc_infosKhaletski.gridheight = 2;
		traitementKhaletski.add(infosKhaletski, gbc_infosKhaletski);
		
		JButton lancerKhaletski = new JButton("Lancer le calcul");
		lancerKhaletski.setPreferredSize(new Dimension(150, 40));
		this.gbc.gridx = 1;
		this.gbc.gridy = 0;
		lancerKhaletski.addActionListener(this.controleur);
		traitementKhaletski.add(lancerKhaletski, this.gbc);

		JButton allerResultatsK = new JButton("Voir r�sultat");
		allerResultatsK.setPreferredSize(new Dimension(150, 40));
		this.gbc.gridx = 1;
		this.gbc.gridy = 1;
		allerResultatsK.addActionListener(this.controleur);
		traitementKhaletski.add(allerResultatsK, this.gbc);

		// M�thode de Khaletski : Encart centre
		JPanel encartG�n�rationKhaletski = new JPanel();
		encartG�n�rationKhaletski.setPreferredSize(new Dimension(750, 550));
		encartG�n�rationKhaletski.setBackground(pecheHiver);
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		ongletKhaletski.add(encartG�n�rationKhaletski, this.gbc);
		encartG�n�rationKhaletski.setLayout(new GridBagLayout());

		this.encartCentreKhaletski = new JPanel();
		this.encartCentreKhaletski.setPreferredSize(new Dimension(750, 550));
		this.encartCentreKhaletski.setBackground(pecheHiver);
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartG�n�rationKhaletski.add(this.encartCentreKhaletski, this.gbc);
		this.encartCentreKhaletski.setLayout(new GridBagLayout());
		this.encartCentreKhaletski.setVisible(false);

		JPanel encartMatriceBK = new JPanel();
		encartMatriceBK.setPreferredSize(new Dimension(240, 500));
		encartMatriceBK.setBackground(pecheHiver);
		this.gbcMatrice.gridx = 0;
		this.gbcMatrice.gridy = 0;
		this.encartCentreKhaletski.add(encartMatriceBK, this.gbcMatrice);
		encartMatriceBK.setLayout(new GridBagLayout());

		JLabel labelMatriceBK = new JLabel("Matrice B :");
		Font font_labelMatriceBK = new Font("Arial", Font.BOLD, 20);
		labelMatriceBK.setFont(font_labelMatriceBK);
		labelMatriceBK.setPreferredSize(new Dimension(240, 50));
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartMatriceBK.add(labelMatriceBK, this.gbc);

		this.cadreMatriceBK = new JPanel();
		this.cadreMatriceBK.setPreferredSize(new Dimension(240, 450));
		this.cadreMatriceBK.setLayout(new GridLayout(1, 1));
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		encartMatriceBK.add(this.cadreMatriceBK, this.gbc);

		JPanel encartMatriceCK = new JPanel();
		encartMatriceCK.setPreferredSize(new Dimension(240, 500));
		encartMatriceCK.setBackground(pecheHiver);
		this.gbcMatrice.gridx = 1;
		this.gbcMatrice.gridy = 0;
		this.encartCentreKhaletski.add(encartMatriceCK, this.gbcMatrice);
		encartMatriceCK.setLayout(new GridBagLayout());

		JLabel labelMatriceCK = new JLabel("Matrice C :");
		Font font_labelMatriceCK = new Font("Arial", Font.BOLD, 20);
		labelMatriceCK.setFont(font_labelMatriceCK);
		labelMatriceCK.setPreferredSize(new Dimension(240, 50));
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartMatriceCK.add(labelMatriceCK, this.gbc);

		this.cadreMatriceCK = new JPanel();
		this.cadreMatriceCK.setPreferredSize(new Dimension(240, 450));
		this.cadreMatriceCK.setLayout(new GridLayout(1, 1));
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		encartMatriceCK.add(this.cadreMatriceCK, this.gbc);

		JPanel encartDeterminantK = new JPanel();
		encartDeterminantK.setPreferredSize(new Dimension(240, 500));
		encartDeterminantK.setBackground(pecheHiver);
		this.gbcMatrice.gridx = 2;
		this.gbcMatrice.gridy = 0;
		this.encartCentreKhaletski.add(encartDeterminantK, this.gbcMatrice);
		encartDeterminantK.setLayout(new GridBagLayout());

		JLabel labelDeterminantK = new JLabel("D�terminant :");
		Font font_labelDeterminantK = new Font("Arial", Font.BOLD, 16);
		labelDeterminantK.setFont(font_labelDeterminantK);
		labelDeterminantK.setPreferredSize(new Dimension(240, 50));
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartDeterminantK.add(labelDeterminantK, this.gbc);

		this.determinantK = new JTextField();
		this.determinantK.setPreferredSize(new Dimension(240, 50));
		Font font_determinantK = new Font("Arial", Font.BOLD, 16);
		this.determinantK.setFont(font_determinantK);
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		encartDeterminantK.add(this.determinantK, this.gbc);

		JButton retourSaisieK = new JButton("Retour � la saisie");
		GridBagConstraints gbc_retourSaisieK = new GridBagConstraints();
		Font font_retourSaisieK = new Font("Arial", Font.BOLD, 16);
		retourSaisieK.setFont(font_retourSaisieK);
		gbc_retourSaisieK.insets = new Insets(20, 0, 20, 0);
		retourSaisieK.setPreferredSize(new Dimension(240, 50));
		gbc_retourSaisieK.gridx = 0;
		gbc_retourSaisieK.gridy = 2;
		retourSaisieK.addActionListener(this.controleur);
		encartDeterminantK.add(retourSaisieK, gbc_retourSaisieK);
		
		// Onglet : R�sultats
		JPanel ongletResultats = new JPanel();
		ongletResultats.setPreferredSize(new Dimension(750, 750));
		ongletResultats.setLayout(new GridBagLayout());
		ongletResultats.setBackground(new Color(250, 241, 231));
		this.onglets.addTab("R�sultat de l'�quation", ongletResultats);

		// R�sultats : Encart haut
		JPanel encartHautResultats = new JPanel();
		encartHautResultats.setPreferredSize(new Dimension(750, 200));
		encartHautResultats.setBackground(songeEte);
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		ongletResultats.add(encartHautResultats, this.gbc);
		encartHautResultats.setLayout(new GridBagLayout());

		JLabel titreResultats = new JLabel("R�sultat de l'�quation");
		Font font_titreResultats = new Font("Arial", Font.BOLD, 26);
		titreResultats.setFont(font_titreResultats);
		titreResultats.setPreferredSize(new Dimension(750, 100));
		titreResultats.setHorizontalAlignment(SwingConstants.CENTER);
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartHautResultats.add(titreResultats, this.gbc);

		JPanel traitementResultats = new JPanel();
		traitementResultats.setPreferredSize(new Dimension(750, 100));
		traitementResultats.setBackground(pecheHiver);
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		encartHautResultats.add(traitementResultats, this.gbc);
		traitementResultats.setLayout(new GridBagLayout());

		this.infosResultats = new JTextArea();
		this.infosResultats.setPreferredSize(new Dimension(500, 80));
		this.infosResultats.setBackground(pecheHiver);
		Font font_infosResultats = new Font("Arial", Font.BOLD, 16);
		infosResultats.setFont(font_infosResultats);
		infosResultats.setEditable(false);
		this.infosResultats.setLineWrap(true);
		GridBagConstraints gbc_infosResultats = new GridBagConstraints();
		gbc_infosResultats.insets = new Insets(10, 20, 10, 20);
		gbc_infosResultats.gridx = 0;
		gbc_infosResultats.gridy = 0;
		traitementResultats.add(this.infosResultats, gbc_infosResultats);

		// R�sultats : Encart centre
		JPanel encartG�n�rationResultats = new JPanel();
		encartG�n�rationResultats.setPreferredSize(new Dimension(750, 550));
		encartG�n�rationResultats.setBackground(pecheHiver);
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		ongletResultats.add(encartG�n�rationResultats, this.gbc);
		encartG�n�rationResultats.setLayout(new GridBagLayout());

		this.encartCentreResultats = new JPanel();
		this.encartCentreResultats.setPreferredSize(new Dimension(750, 550));
		this.encartCentreResultats.setBackground(pecheHiver);
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartG�n�rationResultats.add(this.encartCentreResultats, this.gbc);
		this.encartCentreResultats.setLayout(new GridBagLayout());
		this.encartCentreResultats.setVisible(false);

		JPanel encartMatriceAR = new JPanel();
		encartMatriceAR.setPreferredSize(new Dimension(440, 500));
		encartMatriceAR.setBackground(pecheHiver);
		this.gbcMatrice.gridx = 0;
		this.gbcMatrice.gridy = 0;
		this.encartCentreResultats.add(encartMatriceAR, this.gbcMatrice);
		encartMatriceAR.setLayout(new GridBagLayout());

		JLabel labelMatriceAR = new JLabel("Matrice A :");
		Font font_labelMatriceAR = new Font("Arial", Font.BOLD, 20);
		labelMatriceAR.setFont(font_labelMatriceAR);
		labelMatriceAR.setPreferredSize(new Dimension(440, 50));
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartMatriceAR.add(labelMatriceAR, this.gbc);

		this.cadreMatriceAR = new JPanel();
		this.cadreMatriceAR.setPreferredSize(new Dimension(440, 450));
		this.cadreMatriceAR.setLayout(new GridLayout(1, 1));
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		encartMatriceAR.add(this.cadreMatriceAR, this.gbc);

		JPanel encartVecteurX = new JPanel();
		encartVecteurX.setPreferredSize(new Dimension(140, 500));
		encartVecteurX.setBackground(pecheHiver);
		this.gbcMatrice.gridx = 1;
		this.gbcMatrice.gridy = 0;
		this.encartCentreResultats.add(encartVecteurX, this.gbcMatrice);
		encartVecteurX.setLayout(new GridBagLayout());

		JLabel labelVecteurX = new JLabel("Vecteur X :");
		Font font_labelVecteurX = new Font("Arial", Font.BOLD, 20);
		labelVecteurX.setFont(font_labelVecteurX);
		labelVecteurX.setPreferredSize(new Dimension(140, 50));
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartVecteurX.add(labelVecteurX, this.gbc);

		this.cadreVecteurX = new JPanel();
		this.cadreVecteurX.setPreferredSize(new Dimension(140, 450));
		this.cadreVecteurX.setLayout(new GridLayout(1, 1));
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		encartVecteurX.add(this.cadreVecteurX, this.gbc);

		JPanel encartVecteurBR = new JPanel();
		encartVecteurBR.setPreferredSize(new Dimension(140, 500));
		encartVecteurBR.setBackground(pecheHiver);
		this.gbcMatrice.gridx = 2;
		this.gbcMatrice.gridy = 0;
		this.encartCentreResultats.add(encartVecteurBR, this.gbcMatrice);
		encartVecteurBR.setLayout(new GridBagLayout());

		JLabel labelVecteurBR = new JLabel("Vecteur B :");
		Font font_labelVecteurBR = new Font("Arial", Font.BOLD, 20);
		labelVecteurBR.setFont(font_labelVecteurBR);
		labelVecteurBR.setPreferredSize(new Dimension(140, 50));
		this.gbc.gridx = 0;
		this.gbc.gridy = 0;
		encartVecteurBR.add(labelVecteurBR, this.gbc);

		this.cadreVecteurBR = new JPanel();
		this.cadreVecteurBR.setPreferredSize(new Dimension(140, 450));
		this.cadreVecteurBR.setLayout(new GridLayout(1,1));
		this.gbc.gridx = 0;
		this.gbc.gridy = 1;
		encartVecteurBR.add(this.cadreVecteurBR, this.gbc);
		

	}

	// Methods

	public void reinitialiser() {
		this.cadreMatriceInverse.removeAll();
		this.determinantF.setText("");
		this.polynome.setText("");
		this.encartCentreFaddeev.setVisible(false);
		this.encartCentreKhaletski.setVisible(false);
		this.encartCentreResultats.setVisible(false);
	}

	public JTabbedPane getOnglets() {
		return this.onglets;
	}

	public JTextField getValeurTailleMatrice() {
		return this.valeurTailleMatrice;
	}

	public ControleurProjetMaths getControleur() {
		return this.controleur;
	}

	public JPanel getCadreMatriceA() {
		return this.cadreMatriceA;
	}

	public JPanel getEncartCentreFaddeev() {
		return this.encartCentreFaddeev;
	}

	public JPanel getEncartMilieuSaisie() {
		return this.encartMilieuSaisie;
	}

	public JPanel getEncartBasSaisie() {
		return this.encartBasSaisie;
	}

	public JPanel getCadreVecteurB() {
		return this.cadreVecteurB;
	}

	public JPanel getCadreMatriceInverse() {
		return this.cadreMatriceInverse;
	}

	public JPanel getCadreMatriceB() {
		return this.cadreMatriceBF;
	}

	public JTextField getDeterminant() {
		return this.determinantF;
	}

	public JTextField getPolynome() {
		return this.polynome;
	}

	public JPanel getCadreMatriceBF() {
		return this.cadreMatriceBF;
	}

	public JTextField getDeterminantF() {
		return this.determinantF;
	}

	public JPanel getEncartCentreKhaletski() {
		return this.encartCentreKhaletski;
	}

	public JPanel getCadreMatriceBK() {
		return this.cadreMatriceBK;
	}

	public JPanel getCadreMatriceCK() {
		return this.cadreMatriceCK;
	}

	public JTextField getDeterminantK() {
		return this.determinantK;
	}

	public JPanel getCadreMatriceAR() {
		return this.cadreMatriceAR;
	}

	public JPanel getCadreVecteurX() {
		return this.cadreVecteurX;
	}

	public JPanel getCadreVecteurBR() {
		return this.cadreVecteurBR;
	}

	public JPanel getEncartCentreResultats() {
		return this.encartCentreResultats;
	}
	
	public JTextArea getInfosResultats() {
		return this.infosResultats;
	}

}
