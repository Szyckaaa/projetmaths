package vues;

import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JTextField;

import modele.matrice.MatriceCarre;
import modele.matrice.exception.HorsDePorteeException;

public class VueMatrice extends JPanel {

    // Attributes
    private MatriceCarre   matrice;
    private JTextField[][] coefficients;
    private int            dimCell;

    // Constructor

    // Vue d'une matrice vierge pour la saisie des donn�es
    public VueMatrice( int taille ) throws HorsDePorteeException {
        this.matrice = new MatriceCarre( taille );
        this.setLayout( new GridLayout( taille, taille ) );

        this.coefficients = new JTextField[taille][taille];
        for ( int i = 1; i <= taille; i++ ) {
            for ( int j = 1; j <= taille; j++ ) {
                JPanel cell = new JPanel();
                JTextField txt = new JTextField( "" );
                coefficients[i - 1][j - 1] = txt;
                Font fontTxt = new Font( "Arial", Font.BOLD, 18 );
                txt.setFont( fontTxt );
                txt.setHorizontalAlignment( JTextField.CENTER );
                this.add( cell.add( txt ) );
            }
        }
    }

    // Vue d'une matrice renvoyant les donn�es d'une matrice carr�
    public VueMatrice( MatriceCarre matrice ) {
        this.setLayout( new GridLayout( matrice.taille(), matrice.taille() ) );

        this.coefficients = new JTextField[matrice.taille()][matrice.taille()];
        for ( int i = 1; i <= matrice.taille(); i++ ) {
            for ( int j = 1; j <= matrice.taille(); j++ ) {
                try {
                    JPanel cell = new JPanel();
                    JTextField txt = new JTextField( matrice.getCoefficient( i, j ).toString( false ) );

                    coefficients[i - 1][j - 1] = txt;
                    Font fontTxt = new Font( "Arial", Font.BOLD, 18 );
                    txt.setFont( fontTxt );
                    txt.setHorizontalAlignment( JTextField.CENTER );
                    this.add( cell.add( txt ) );
                    txt.setEditable( false );
                } catch ( HorsDePorteeException e ) {
                    e.printStackTrace();
                }
            }
        }
    }

    public JTextField getCoefficent( int i, int j ) {
        return this.coefficients[i - 1][j - 1];
    }

    // Methods
    public MatriceCarre getMatrice() {
        return matrice;
    }

}
