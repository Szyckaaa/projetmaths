package controleur;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JTextField;

import modele.fraction.Fraction;
import modele.fraction.exception.InversionNulException;
import modele.matrice.MatriceCarre;
import modele.matrice.exception.HorsDePorteeException;
import modele.matrice.exception.IninversibleException;
import modele.matrice.exception.OperationInvalideException;
import vues.VueMatrice;
import vues.VueProjetMaths;
import vues.VueVecteur;

public class ControleurProjetMaths implements ActionListener {

    // Attributes
    private VueProjetMaths vue;
    private MatriceCarre   modele;
    private Fraction[]     vecteurB;

    private VueMatrice     matriceSaisie;
    private VueVecteur     vecteurSaisie;

    // Constructor
    public ControleurProjetMaths( VueProjetMaths vue ) {
        this.vue = vue;
    }

    // Methods
    @Override
    public void actionPerformed( ActionEvent evt ) throws IllegalArgumentException {
        JButton b = (JButton) evt.getSource();

        if ( b.getText().equals( "C'est parti !" ) ) {
            this.vue.getOnglets().setSelectedIndex( 1 );
        }
        
        if ( b.getText().equals( "Voir r�sultat" ) ) {
        	
        	this.vue.getOnglets().setSelectedIndex( 4 );
        }
        
        if ( b.getText().equals( "Retour � la saisie" ) ) {
        	
        	this.vue.getOnglets().setSelectedIndex( 1 );
        }

        if ( b.getText().equals( "G�n�rer" ) ) {

            this.vue.reinitialiser();
            this.modele = null;
            this.vecteurB = null;

            if ( this.vue.getValeurTailleMatrice().getText().equals( "" ) ) {

                throw new IllegalArgumentException();
            }

            if ( this.vue.getCadreMatriceA().getComponentCount() != 0 ) {

                this.vue.getCadreMatriceA().removeAll();
                this.vue.getCadreVecteurB().removeAll();
            }

            try {
                this.vue.getEncartMilieuSaisie().setVisible( true );
                this.vue.getEncartBasSaisie().setVisible( true );

                int tailleMatrice = Integer.parseInt( this.vue.getValeurTailleMatrice().getText() );

                this.matriceSaisie = new VueMatrice( tailleMatrice );
                this.vue.getCadreMatriceA().add( matriceSaisie );

                this.vecteurSaisie = new VueVecteur( tailleMatrice );
                this.vue.getCadreVecteurB().add( this.vecteurSaisie );

            } catch ( NumberFormatException | HorsDePorteeException e ) {
                e.printStackTrace();
            }
        }

        if ( b.getText().equals( "M�thode de Faddeev" ) ) {
            this.vue.getOnglets().setSelectedIndex( 2 );
        }

        if ( b.getText().equals( "M�thode de Khaletski" ) ) {
            this.vue.getOnglets().setSelectedIndex( 3 );
        }

        if ( b.getText().equals( "Lancer le traitement" ) ) {
            this.traiterSaisie();
            this.traiterVecteur();

            // r�initialise les cadres
            this.vue.getCadreMatriceInverse().removeAll();
            this.vue.getCadreMatriceAR().removeAll();
            this.vue.getCadreVecteurX().removeAll();
            this.vue.getCadreVecteurBR().removeAll();

            // Lancer faddeev sur le modele
            try {

                this.modele.fadeev();
                VueMatrice matriceInverseF = new VueMatrice( this.modele.getInverse() );
                this.vue.getCadreMatriceInverse().add( matriceInverseF );
                // Mise en place des r�sultats dans le dernier onglet
                Fraction[] X = new Fraction[this.modele.taille()];
                try {
                    X = MatriceCarre.multiplier( this.modele.getInverse(), this.vecteurB );
                } catch ( NullPointerException | OperationInvalideException | HorsDePorteeException e ) {
                    e.printStackTrace();
                }

                this.creerVueResultat( X );
            } catch ( IninversibleException e ) {
                // ajout de champs pr�cisant que la matrice est non inversible
                // dans les champs n�cessitant la matrice inverse
                this.vue.getCadreMatriceInverse().add( new JTextField( "Matrice non inversible" ) );
                ( (JTextField) this.vue.getCadreMatriceInverse().getComponent( 0 ) ).setEditable( false );
                this.vue.getCadreMatriceAR().add( new JTextField( "Matrice non inversible" ) );
                ( (JTextField) this.vue.getCadreMatriceAR().getComponent( 0 ) ).setEditable( false );
                this.vue.getCadreVecteurX().add( new JTextField( "Matrice non inversible" ) );
                ( (JTextField) this.vue.getCadreVecteurX().getComponent( 0 ) ).setEditable( false );
                this.vue.getCadreVecteurBR().add( new JTextField( "Matrice non inversible" ) );
                ( (JTextField) this.vue.getCadreVecteurBR().getComponent( 0 ) ).setEditable( false );

            }

            // afficher les champs de resultats qui sont cach�s
            this.vue.getEncartCentreFaddeev().setVisible( true );
            this.vue.getEncartCentreResultats().setVisible( true );

            this.vue.getPolynome().setText(" " + polynomeToString(this.modele.getPolCaracteristique() ) );
            this.vue.getPolynome().setEditable( false );
            this.vue.getDeterminantF().setText(" " + this.modele.getDeterminant().toString(false) );
            this.vue.getDeterminantF().setEditable( false );

            // TODO Ajouter comment est calculer le r�sultat dans le JTextField
            // de r�sultat
            String msgResultat = "\n" + "Le r�sultat est obtenu en effectuant le produit matriciel A.X = b.";
            this.vue.getInfosResultats().setText(msgResultat);
        }

        if ( b.getText().equals( "Lancer le calcul" ) ) {
            this.vue.getCadreMatriceBK().removeAll();
            this.vue.getCadreMatriceCK().removeAll();

            this.traiterSaisie();
            this.traiterVecteur();

            // affiche les champs de r�sultat cach�s
            this.vue.getEncartCentreKhaletski().setVisible( true );
            this.vue.getEncartCentreResultats().setVisible( true );

            MatriceCarre[] resultatKhal = new MatriceCarre[2];
            try {
                resultatKhal = this.modele.khaletski( this.vecteurB );
            } catch ( HorsDePorteeException | InversionNulException | OperationInvalideException e ) {
                e.printStackTrace();
            }

            MatriceCarre khalb = resultatKhal[0];
            MatriceCarre khalc = resultatKhal[1];

            this.vue.getCadreMatriceBK().add( new VueMatrice( khalb ) );
            this.vue.getCadreMatriceCK().add( new VueMatrice( khalc ) );
            this.vue.getDeterminantK().setText(" " + this.modele.getDeterminant().toString(false) );
            this.vue.getDeterminantK().setEditable( false );
            // R�solution de l'�quation
            Fraction[] y;
            Fraction[] X = new Fraction[this.modele.taille()];
            try {
                y = MatriceCarre.solveTriangInf( khalb, this.vecteurB );
                X = MatriceCarre.solveTriangSup( khalc, y );
            } catch ( HorsDePorteeException | OperationInvalideException e ) {
                e.printStackTrace();
            }

            this.creerVueResultat( X );

            // TODO Ajouter comment est calculer le r�sultat dans le JTextField
            // de r�sultat
            String msgResultat = "\n" + "Le r�sultat est obtenu en r�solvant deux �quation impliquant les matrices"
            		+ " triangulaires calcul�s pr�c�demment." 
            		+ "\n" + "Il faut r�soudre B.y = b puis C.X = y \n";
            this.vue.getInfosResultats().setText(msgResultat);

        }

        this.vue.repaint();
        
    }

    private void creerVueResultat( Fraction[] X ) {
        this.vue.getCadreMatriceAR().removeAll();
        this.vue.getCadreVecteurX().removeAll();
        this.vue.getCadreVecteurBR().removeAll();

        this.vue.getCadreMatriceAR().add( new VueMatrice( this.modele ) );
        VueVecteur vueResultatX = new VueVecteur( this.modele.taille() );
        for ( int i = 1; i <= this.modele.taille(); i++ )
            vueResultatX.setComposante( i, X[i - 1] );
        this.vue.getCadreVecteurX().add( vueResultatX );

        VueVecteur vueResultatB = new VueVecteur( this.modele.taille() );
        for ( int i = 1; i <= this.modele.taille(); i++ )
            vueResultatB.setComposante( i, this.vecteurB[i - 1] );
        this.vue.getCadreVecteurBR().add( vueResultatB );
    }

    private void traiterSaisie() {
        // Cr�ation de la matrice de saisie
        int tailleMatrice = Integer.parseInt( this.vue.getValeurTailleMatrice().getText() );
        Fraction[] coeffSaisie = new Fraction[tailleMatrice * tailleMatrice];

        // R�initialise la couleur des cases de saisie � blanc
        for ( int i = 1; i <= tailleMatrice; i++ )
            for ( int j = 1; j <= tailleMatrice; j++ ) {
                this.matriceSaisie.getCoefficent( i, j ).setBackground( Color.WHITE );
            }

        // parsage des coefficients
        try {
            for ( int i = 1; i <= tailleMatrice; i++ ) {
                for ( int j = 1; j <= tailleMatrice; j++ ) {
                    try {
                        coeffSaisie[tailleMatrice * ( i - 1 ) + j - 1] = parserSaisieCase(
                                this.matriceSaisie.getCoefficent( i, j ).getText() );
                    } catch ( IllegalArgumentException e ) {
                        // Met la case en d�faut � rouge
                        this.matriceSaisie.getCoefficent( i, j ).setBackground( Color.RED );
                        throw new IllegalArgumentException( "Un coefficient est incorrect" );
                    }
                }
            }
        } catch ( IllegalArgumentException e ) {
            // renvoie vers l'onglet de saisie en cas d'erreur
            this.vue.getOnglets().setSelectedIndex( 1 );
        }

        // Initialisation du modele
        this.modele = new MatriceCarre( tailleMatrice );
        try {
            this.modele.setCoefficients( coeffSaisie );
        } catch ( Exception e ) {
            e.printStackTrace();
        }

    }

    private void traiterVecteur() {
        // Cr�ation de la matrice de saisie
        int tailleMatrice = Integer.parseInt( this.vue.getValeurTailleMatrice().getText() );
        this.vecteurB = new Fraction[tailleMatrice];

        // R�initialise la couleur des cases de saisie � blanc
        for ( int i = 1; i <= tailleMatrice; i++ ) {
            this.vecteurSaisie.getComposante( i ).setBackground( Color.WHITE );
        }

        // parsage des composante
        try {
            for ( int i = 1; i <= tailleMatrice; i++ ) {
                try {
                    this.vecteurB[i - 1] = parserSaisieCase( this.vecteurSaisie.getComposante( i ).getText() );
                } catch ( IllegalArgumentException e ) {
                    // Met la case en d�faut � rouge
                    this.vecteurSaisie.getComposante( i ).setBackground( Color.RED );
                    throw new IllegalArgumentException( "Une composante est incorrect" );
                }
            }
        } catch ( IllegalArgumentException e ) {
            // renvoie vers l'onglet de saisie en cas d'erreur
            this.vue.getOnglets().setSelectedIndex( 1 );
        }
    }

    public Fraction parserSaisieCase( String saisie ) throws IllegalArgumentException {
        Fraction result = null;
        if ( Pattern.matches( "[-]?[0-9]{1,9}([/]?[1-9]{1,9})?", saisie ) ) {
            if ( Pattern.matches( "[-]?[0-9]{1,9}", saisie ) ) {
                result = new Fraction( Integer.parseInt( saisie ) );
            } else {
                int posSlash = saisie.indexOf( "/" );
                result = new Fraction( Integer.parseInt( saisie.substring( 0, posSlash ) ),
                        Integer.parseInt( saisie.substring( posSlash + 1 ) ) );
            }

        } else {
            throw new IllegalArgumentException( "La saisie ne permet pas de g�n�rer une Fraction." );
        }
        return result;
    }

    public String polynomeToString( Fraction[] polynome ) throws IllegalArgumentException {
        if ( polynome.length == 0 || polynome == null )
            throw new IllegalArgumentException( "Le polynome est vide" );
        String result = "";

        int i = polynome.length - 1;
        Fraction zero = new Fraction( 0 );
        for ( Fraction f : polynome ) {
            if ( !f.equals( zero ) ) {
                if ( f.getSigne() == 1 && i != polynome.length - 1 )
                    result += "+";

                if ( !( f.getNumerator() == 1 && f.getDenominator() == 1 ) )
                    result += f.toString( false );

                if ( i != 0 ) {
                    result += "x";
                    if ( i != 1 )
                        result += "^" + i;
                }

                result += "  ";
            }
            i--;
        }
        return result;
    }

}